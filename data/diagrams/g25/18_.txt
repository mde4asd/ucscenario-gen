
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Object" as thingobject
	entity "Title" as thingtitle
	actor "Repository Manager" as actorrepositorymanager
	boundary "Title\nInterface" as thingtitleinterface #grey
	control "Describe An\nObject Including\nTitle" as controldescribeobject

	thingtitle <.. thingobject
	actorrepositorymanager --- thingtitleinterface
	thingobject --- controldescribeobject
	thingtitle --- controldescribeobject
	thingtitleinterface --> controldescribeobject

@enduml
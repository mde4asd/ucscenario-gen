
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Search Criteria" as thingsearchcriteria #grey
	entity "Search Option" as thingsearchoption
	actor "Site Visitor" as actorsitevisitor
	circle "Form" as thingform
	boundary "Criteria\nInterface" as thingcriteriainterface #grey
	control "Have An\nAdvanced Search\nOption Lets\nFill A\nForm Of\nSearch Criteria" as controlhaveadvancedsearchoption

	thingform <.. thingsearchoption
	thingsearchcriteria *-- thingform
	actorsitevisitor --- thingcriteriainterface
	thingsearchoption --- controlhaveadvancedsearchoption
	thingform --- controlhaveadvancedsearchoption
	thingsearchcriteria --- controlhaveadvancedsearchoption
	thingcriteriainterface --> controlhaveadvancedsearchoption

@enduml
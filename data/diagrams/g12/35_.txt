
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Activity" as thingactivity
	entity "Child" as thingchild
	entity "Camp" as thingcamp
	entity "Schedule" as thingschedule
	actor "Parent" as actorparent
	boundary "Camp\nInterface" as thingcampinterface #grey
	boundary "Schedule\nInterface" as thingscheduleinterface #grey
	control "Track Child's\nActivity At\nCamp" as controltrackactivitycamp
	control "Track Schedule" as controltrackschedule

	thingchild <.. thingactivity
	thingcamp <.. thingactivity
	actorparent --- thingcampinterface
	actorparent --- thingscheduleinterface
	thingactivity --- controltrackactivitycamp
	thingcamp --- controltrackactivitycamp
	thingcampinterface --> controltrackactivitycamp
	thingschedule --- controltrackschedule
	thingscheduleinterface --> controltrackschedule

@enduml
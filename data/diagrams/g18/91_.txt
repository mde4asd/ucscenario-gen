
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Format" as thingformat
	entity "Ability" as thingability
	entity "File" as thingfile
	actor "Researcher" as actorresearcher
	boundary "File Type" as thingfiletyperesearcher
	boundary "Format\nInterface" as thingformatinterface #grey
	control "Have The\nTo Ability\nFor Files\nBy" as controlhaveability
	control "Have The\nTo Ability\nFor Files\nBy Format" as controlhaveabilityformat

	thingfile <.. thingability
	thingformat <.. thingfile
	actorresearcher --- thingfiletyperesearcher
	actorresearcher --- thingformatinterface
	thingability --- controlhaveability
	thingfile --- controlhaveability
	thingfiletyperesearcher --> controlhaveability
	thingformat --- controlhaveabilityformat
	thingformatinterface --> controlhaveabilityformat
	thingability --- controlhaveabilityformat
	thingfile --- controlhaveabilityformat

@enduml
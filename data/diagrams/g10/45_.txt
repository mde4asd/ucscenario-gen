
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Job" as thingjob
	entity "Rss Feed" as thingrssfeed #grey
	actor "Site Member" as actorsitemember
	boundary "Job\nInterface" as thingjobinterface #grey
	control "Subscribe To\nAn Rss\nFeed Of\nJobs" as controlsubscriberssfeed

	thingjob <.. thingrssfeed
	actorsitemember --- thingjobinterface
	thingrssfeed --- controlsubscriberssfeed
	thingjob --- controlsubscriberssfeed
	thingjobinterface --> controlsubscriberssfeed

@enduml
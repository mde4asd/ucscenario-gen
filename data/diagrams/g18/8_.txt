
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Experiment" as thingexperiment
	entity "File" as thingfile
	entity "Page" as thingpage
	entity "Log Book" as thinglogbook #grey
	actor "Researcher" as actorresearcher
	boundary "Experiment\nInterface" as thingexperimentinterface #grey
	boundary "Page\nInterface" as thingpageinterface #grey
	control "Create A\nLog Book\nPage For\nAn Experiment" as controlcreatelogbookpage
	control "Attach A\nFile To\nThis Page" as controlattachfilepage

	thingpage <.. thingfile
	thinglogbook <.. thingpage
	thingexperiment <.. thingpage
	actorresearcher --- thingexperimentinterface
	actorresearcher --- thingpageinterface
	thingexperiment --- controlcreatelogbookpage
	thingexperimentinterface --> controlcreatelogbookpage
	thingpage --- controlcreatelogbookpage
	thingfile --- controlattachfilepage
	thingpage --- controlattachfilepage
	thingpageinterface --> controlattachfilepage

@enduml
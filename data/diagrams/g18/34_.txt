
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Access" as thingaccess
	entity "System" as thingsystem
	entity "Regular User" as thingregularuser
	actor "Lab Administrator" as actorlabadministrator
	boundary "System\nInterface" as thingsysteminterface #grey
	control "Support Regular\nUsers" as controlsupportregularuser
	control "Increased Access\nTo The\nSystem" as controlincreaseaccesssupportregularuser

	thingsystem <.. thingaccess
	actorlabadministrator --- thingsysteminterface
	thingregularuser --- controlsupportregularuser
	controlincreaseaccesssupportregularuser --> controlsupportregularuser
	thingsysteminterface --> controlsupportregularuser
	thingaccess --- controlincreaseaccesssupportregularuser
	thingsystem --- controlincreaseaccesssupportregularuser
	thingsysteminterface --> controlincreaseaccesssupportregularuser

@enduml

@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Volunteer" as thingvolunteer
	actor "Zooniverse Admin" as actorzooniverseadmin
	boundary "Volunteer\nInterface" as thingvolunteerinterface #grey
	control "Interrupt A\nVolunteer" as controlinterruptvolunteer
	control "Know" as controlknowinterruptvolunteer

	actorzooniverseadmin --- thingvolunteerinterface
	thingvolunteer --- controlinterruptvolunteer
	controlknowinterruptvolunteer --> controlinterruptvolunteer
	thingvolunteerinterface --> controlinterruptvolunteer
	thingvolunteerinterface --> controlknowinterruptvolunteer

@enduml
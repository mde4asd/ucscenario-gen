from typing import Optional, Iterable, Callable, Dict, Set, Type
from importlib.util import find_spec
from contextlib import redirect_stdout
from io import StringIO
from spacy.tokens.doc import Doc
from spacy.tokens.token import Token
from spacy.lang.en.stop_words import STOP_WORDS

use_src = find_spec("src")

if use_src:
    from src.storyparser.entity import *
    from src.storyparser.rules import rules, Carryover, Flag
    from src.storyparser.node import Node
    from src.util import pretty_print, Logger
    from src.preprocessing.preprocessing import split_us

else:
    from storyparser.entity import *
    from storyparser.rules import rules, Carryover, Flag
    from storyparser.node import Node
    import sys
    sys.path.insert(0, "../")
    from util import pretty_print, Logger
    from preprocessing.preprocessing import split_us


class TreeParser:
    """
    For parsing dependency trees.
    """

    def __init__(self):
        self.all_ents = []
        self.ents_by_story = []

    def entities(self, doc: Doc, logger: Logger, nlp) -> Tuple[List[Entity], List[Entity]]:
        tree = self._doc_to_node(doc)

        stdout = StringIO()
        pretty_print(tree, text_func=lambda t: "%s--[%s]==(%s)~~|%s|" % (t.word, t.arc, t.pos_fine, t.lemma))
        with redirect_stdout(stdout):
            print("word---[universal dependency tag]===(part of speech tag)~~~|lemma|")
            pretty_print(tree, text_func=lambda t: "%s--[%s]==(%s)~~|%s|" % (t.word, t.arc, t.pos_fine, t.lemma))
        logger.log("Dependency Tree", stdout.getvalue())

        split = split_us(doc.text)

        actor_node = self._doc_to_node(nlp(split[0]))[0]
        control_node = self._doc_to_node(nlp(split[1]))[0]

        entities: Set[Entity] = set()
        # parsing actors
        carryover = Carryover()
        parse_result = self._parse_node(actor_node, None, carryover,
                                        default=EntityType.actor, include_relations=False)
        primary_actors = [ent for ent in parse_result if ent.entity_type == EntityType.actor]
        for ent in parse_result: entities.add(ent)

        # parsing controls
        parse_result = self._parse_node(control_node, None, carryover.clear_flags(), default=EntityType.object)
        for ent in parse_result: entities.add(ent)

        # parsing usecase
        """
        if usecase_node:
            parse_result = self._parse_node(usecase_node, None, carryover.clear_flags(),
                                            default=EntityType.object, assume_entities=False)
            for ent in parse_result: entities.add(ent)
        """   
        
        
        stdout = StringIO()
        roots = [e for e in entities if not e.parent]

        pretty_print(roots, text_func=lambda e: str(e), child_func=lambda e: e.relations)
        with redirect_stdout(stdout):
            pretty_print(roots, text_func=lambda e: str(e), child_func=lambda e: e.relations)
        logger.log("After Dependency Tree Parsing", stdout.getvalue())
        
       
        primary_actors = TreeParser._remove_noise_entity(primary_actors)
        entities = TreeParser._remove_noise_entity(entities)
        
        entities = TreeParser._connect_actor_interface(entities)

        entities = self.process_all(entities, primary_actors)

        stdout = StringIO()
        roots = [e for e in entities if not e.parent]

        pretty_print(roots, text_func=lambda e: str(e), child_func=lambda e: e.relations)
        with redirect_stdout(stdout):
            pretty_print(roots, text_func=lambda e: str(e), child_func=lambda e: e.relations)
        logger.log("After Post-Processing of Parser Output", stdout.getvalue())

        self.ents_by_story.append((list(entities), primary_actors))
        self.all_ents += self.ents_by_story[-1][0]

        return self.ents_by_story[-1][0], primary_actors

    
    @staticmethod
    def _extract_key_nodes(tree):
        """
        Return the 3 mains part of the US :
            actor_node : first part US with "AS a"
            control_node : second part US with "I Want"
            usecase_node : last part US with "So that"
        """
        actor_node, control_node, usecase_node = None, None, None
        for node in tree:
            if not actor_node:
                actor_node = TreeParser.traverse(node, lambda n: TreeParser.is_phrase(n, "as", "a"))
                if not actor_node:
                    actor_node = TreeParser.traverse(node, lambda n: TreeParser.is_phrase(n, "as", "an"))
            if not control_node:
                control_node = TreeParser.traverse(node, lambda n: TreeParser.is_phrase(n, "i", "want"))
                if not actor_node:
                    actor_node = TreeParser.traverse(node, lambda n: TreeParser.is_phrase(n, "i", "would"))
            #if not usecase_node:
             #   usecase_node = TreeParser.traverse(node, lambda n: TreeParser.is_phrase(n, "so", "that"))
        assert actor_node and control_node
        
        for root_node in [x for x in [actor_node, control_node, usecase_node] if x is not None]:
            if root_node.parent:
                root_node.parent.remove_modifier(root_node)
        return actor_node, control_node #, usecase_node

   
    @staticmethod
    def _parse_node(node: Node, parent: Entity, carryover: Carryover, result: List[Entity] = None,
                    default: EntityType = EntityType.extension, assume_entities: bool = True,
                    include_relations: bool = True) -> List[Entity]:
        """
        Nous prenons un noeud et nous allons le parser lui et la liste des enfants que la règle à besoin de parser. Pour chaque noeud, on va le parser avec la seul règle qui match.
        Si aucune règle ne match, nous passons directement à ses enfants.
        L'effet des règles peut varier selon le EntityType.
        Renvoie la liste des entitys parsées
        """
        # Not all conditions are covered. Flag keeps dfs processing if nothing is found
        rule_met = False

        if result is None:
            result = []

        # Assert to identify if multiple rules trigger (they shouldn't)
        passes = [r for r in rules if r.check_conditions(node)]
        if len(passes) > 1:
            print(node.word, "triggered", [str(r) for r in passes])
            assert len(passes) <= 1

        for rule in rules:

            if rule.check_conditions(node) and not node.ignore:
               
                rule_met = True
                entity, new, carryover, nodes = rule.parse(node, parent, default, assume_entities, carryover)
                
                assume_entities = assume_entities and not carryover.check(Flag.statement)
                result += new
                for n in nodes:
                    TreeParser._parse_node(n, entity, carryover, result=result,
                                           default=default, assume_entities=assume_entities,
                                           include_relations=include_relations)

        if not rule_met:
            for mod in node.modifiers:
                TreeParser._parse_node(mod, parent, carryover, result=result,
                                       default=default, assume_entities=assume_entities,
                                       include_relations=include_relations)

        return result
    

    @staticmethod
    def process_all(entities, primary_actors) -> Set[Entity]:
        new_entities: List[Entity] = []

        for entity in entities:
                if entity.entity_type == EntityType.primary_actor_proxy:
                    if entity.parent:
                        for actor in primary_actors:
                            duplicate_actor, new = actor.duplicate(dup_up=False, dup_down=False)
                            entity.add_relation(duplicate_actor)
                            new_entities += new

                elif entity.entity_type == EntityType.object_proxy:
                    # proxies now down in rules. Except for primary actor.
                    pass
        return entities.union(new_entities)

    @staticmethod
    def traverse(node: Node, on_traversal: Callable) -> Optional[Node]:
        result = on_traversal(node)
        if result:
            return result
        else:
            for mod in node.modifiers:
                result = TreeParser.traverse(mod, on_traversal)
                if result:
                    return result
        return None

    
    @staticmethod
    def is_phrase(node: Node, k1: str, k2: str) -> Optional[Node]:
        """
        if k1 and k2 found in children_node.word => the first children_nodes == k1 or k2 => children_node.ignore = True
        return node if k1 and k2 is in node.word or children_node.word 
        return node.parent if k1 or k2 is in children_node.word and the other is in node.parent.word
        Otherwise None
        """
        m1 = [n for n in node.modifiers if n.word.lower() == k1.lower()]
        m2 = [n for n in node.modifiers if n.word.lower() == k2.lower()]
        child_k1 = m1[0] if m1 else None
        child_k2 = m2[0] if m2 else None
        if child_k1 and child_k2:
            child_k1.ignore, child_k2.ignore = True, True
        if node.word.lower() == k1.lower() and child_k2:
            return node
        elif node.word.lower() == k2.lower() and child_k1:
            return node
        elif child_k1 and child_k2:
            return node
        elif node.parent:
            if node.parent.word.lower() == k1.lower():
                if child_k2:
                    return node.parent
            elif node.parent and node.parent.word.lower() == k2.lower():
                if child_k1:
                    return node.parent
        return None

    @staticmethod
    def _doc_to_node(doc) -> Iterable[Node]:
        """
        Convert doc from spacy to Node_Tree 
        """ 
        
        nodes = []
        for token in doc:
            if(token.dep_ =="ROOT"):
                nodes.append(TreeParser._make_nodes(token, None))
        return nodes
   

    @staticmethod
    def _make_nodes(token: Token, parent: Optional[Node]) -> Node:
        """
        Convert Token in Node
        """
        children = []
        n = Node(token, parent)
        for child in token.children:
            children.append(TreeParser._make_nodes(child, n))
        n.add_modifiers(children)
        return n

    def _remove_noise_entity(list_ent: List[Entity]):
        """
        We remove all object entities that are stop_words or noise_word because it's only noise
        and we connect all their children to their parent.
        """
        noises_words = ["list","set"]

        list_ent = list(list_ent)
        for i in range(0,len(list_ent)): 
            ent = list_ent[i]
            if  ent.entity_type in [EntityType.object,EntityType.object_proxy,EntityType.interface] and (ent.lemma in STOP_WORDS or ent.lemma in noises_words) :
                parent = ent.parent
                for c in ent.children:
                    parent.add_child(c)
                list_ent[i] = None
       
        return set([ent for ent in list_ent if ent != None])
            
    
    def _connect_actor_interface(list_ent: List[Entity]):
        """
        We connect all interfaces generated for a US to the actors of that US. 
        To be able to connect the same actor to several interfaces we will have to duplicate it. 
        
        The list of entities is returned by adding duplicated actors and relationships between actors and interfaces
        """
        results = []
        list_actor = [e for e in list_ent if e.entity_type == EntityType.actor]

        for ent in list_ent:    
            if(ent.entity_type == EntityType.interface):
                for act in list_actor:
                    if (act._parent):
                        dup_act, _ = act.duplicate()
                        ent.add_relation(dup_act)  
                        results.append(dup_act)
                    else : 
                        ent.add_relation(act)  
                        
            results.append(ent)
            
        return set(results)

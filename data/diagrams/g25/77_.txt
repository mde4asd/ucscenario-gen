
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Collection" as thingcollection
	entity "Dam" as thingdams
	entity "Description" as thingdescription
	entity "Object" as thingobject
	entity "Resource" as thingresource
	actor "Repository Manager" as actorrepositorymanager
	circle "Pertinent ,\nVersion" as thingpertinentotherversion
	circle "Component" as thingcomponent #grey
	boundary "Dams\nInterface" as thingdamsinterface #grey
	boundary "Collection\nInterface" as thingcollectioninterface #grey
	boundary "Object\nInterface" as thingobjectinterface #grey
	boundary "Description\nInterface" as thingdescriptioninterface #grey
	control "Indicate All\nPertinent, Other\nVersions Of\nA Resource\nAre External\nTo The\nDams" as controlindicatepertinentotherversion
	control "Indicate All\nPertinent, Other\nVersions Of\nE.G. Collection" as controlindicatepertinentotherversioncollection
	control "Indicate All\nPertinent, Other\nVersions Of\nObject" as controlindicatepertinentotherversionobject
	control "Indicate Description" as controlindicatedescription
	control "Indicate All\nPertinent, Other\nVersions Of\nObject Component" as controlindicatepertinentotherversionobjectcomponent

	thingdams <.. thingresource
	thingresource *-- thingpertinentotherversion
	thingdams *-- thingpertinentotherversion
	thingcomponent *-- thingpertinentotherversion
	thingcollection *-- thingpertinentotherversion
	thingobject *-- thingpertinentotherversion
	thingobject *-- thingcomponent
	actorrepositorymanager --- thingdamsinterface
	actorrepositorymanager --- thingcollectioninterface
	actorrepositorymanager --- thingobjectinterface
	actorrepositorymanager --- thingdescriptioninterface
	thingpertinentotherversion --- controlindicatepertinentotherversion
	thingresource --- controlindicatepertinentotherversion
	thingdams --- controlindicatepertinentotherversion
	thingdamsinterface --> controlindicatepertinentotherversion
	thingcollection --- controlindicatepertinentotherversioncollection
	thingcollectioninterface --> controlindicatepertinentotherversioncollection
	thingpertinentotherversion --- controlindicatepertinentotherversioncollection
	thingobject --- controlindicatepertinentotherversionobject
	thingobjectinterface --> controlindicatepertinentotherversionobject
	thingpertinentotherversion --- controlindicatepertinentotherversionobject
	thingdescription --- controlindicatedescription
	thingdescriptioninterface --> controlindicatedescription
	thingcomponent --- controlindicatepertinentotherversionobjectcomponent
	thingpertinentotherversion --- controlindicatepertinentotherversionobjectcomponent
	thingobjectinterface --> controlindicatepertinentotherversionobjectcomponent

@enduml
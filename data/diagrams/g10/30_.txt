
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Course" as thingcourse
	entity "Event" as thingevent
	actor "Site Admin" as actorsiteadmin
	boundary "Event\nInterface" as thingeventinterface #grey
	boundary "Course\nInterface" as thingcourseinterface #grey
	control "Delete Event" as controldeleteevent
	control "Delete Any\nCourse" as controldeletecourse

	actorsiteadmin --- thingeventinterface
	actorsiteadmin --- thingcourseinterface
	thingevent --- controldeleteevent
	thingeventinterface --> controldeleteevent
	thingcourse --- controldeletecourse
	thingcourseinterface --> controldeletecourse

@enduml
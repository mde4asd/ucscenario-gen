
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Effective Manner" as thingeffectivemanner
	entity "Collection" as thingcollection
	entity "Repository" as thingrepository
	actor "Researcher" as actorresearcher
	circle "Subset" as thingsubset
	boundary "Collection\nInterface" as thingcollectioninterface #grey
	control "Search Within\nA Repository\nIn An\nEffective Manner\nSearching Within\nSubsets Of\nA Given\nCollection" as controlsearchrepository
	control "Search Within\nCollection" as controlsearchcollection

	thingsubset <.. thingeffectivemanner
	thingeffectivemanner <.. thingrepository
	thingcollection *-- thingsubset
	actorresearcher --- thingcollectioninterface
	thingrepository --- controlsearchrepository
	thingeffectivemanner --- controlsearchrepository
	thingsubset --- controlsearchrepository
	thingcollectioninterface --> controlsearchrepository
	thingcollection --- controlsearchrepository
	thingcollection --- controlsearchcollection
	thingcollectioninterface --> controlsearchcollection

@enduml
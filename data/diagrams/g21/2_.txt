
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Website" as thingwebsite
	entity "Info" as thinginfo
	entity "Trainer" as thingtrainer
	entity "Update" as thingupdate
	actor "Trainingcoordinator" as actortrainingcoordinator
	boundary "Info\nInterface" as thinginfointerface #grey
	boundary "Update\nInterface" as thingupdateinterface #grey
	control "Email The\nTrainers Once\nFrom The\nWebsite For\nInfo" as controlemailtrainerwebsite
	control "Email The\nTrainers Once\nFrom The\nWebsite For\nUpdate" as controlemailtrainerwebsiteupdate

	thinginfo <.. thingwebsite
	thingupdate <.. thingwebsite
	thingwebsite <.. thingtrainer
	actortrainingcoordinator --- thinginfointerface
	actortrainingcoordinator --- thingupdateinterface
	thingtrainer --- controlemailtrainerwebsite
	thingwebsite --- controlemailtrainerwebsite
	thinginfo --- controlemailtrainerwebsite
	thinginfointerface --> controlemailtrainerwebsite
	thingupdate --- controlemailtrainerwebsiteupdate
	thingupdateinterface --> controlemailtrainerwebsiteupdate
	thingwebsite --- controlemailtrainerwebsiteupdate
	thingtrainer --- controlemailtrainerwebsiteupdate

@enduml

@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Dataset Offline" as thingdatasetoffline #grey
	actor "Developer" as actordeveloper
	boundary "Offline\nInterface" as thingofflineinterface #grey
	control "Take A\nDataset Offline" as controltakedatasetoffline

	actordeveloper --- thingofflineinterface
	thingdatasetoffline --- controltakedatasetoffline
	thingofflineinterface --> controltakedatasetoffline

@enduml
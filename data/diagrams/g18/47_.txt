
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Result" as thingresult #grey
	entity "Search Results" as thingsearchresults #grey
	entity "Log Books" as thinglogbooks #grey
	entity "Log Book\nSections" as thinglogbooksections #grey
	entity "Section" as thingsection #grey
	actor "User" as actoruser
	boundary "Result\nInterface" as thingresultinterface #grey
	boundary "Books\nInterface" as thingbooksinterface #grey
	boundary "Section\nInterface" as thingsectioninterface #grey
	control "Limit Results\nTo More" as controllimitresultmore
	control "Limit Search\nResults To\nLog Book\nSections From\nLog Books" as controllimitsearchresultslogbooksections
	control "Limit Results\nTo Sections\nFrom More" as controllimitresultsectionmore

	thingsection <.. thingresult
	thinglogbooksections <.. thingsearchresults
	thinglogbooks <.. thinglogbooksections
	actoruser --- thingresultinterface
	actoruser --- thingbooksinterface
	actoruser --- thingsectioninterface
	thingresultinterface --> controllimitresultmore
	thingresult --- controllimitresultmore
	thingsearchresults --- controllimitsearchresultslogbooksections
	thinglogbooksections --- controllimitsearchresultslogbooksections
	thinglogbooks --- controllimitsearchresultslogbooksections
	thingbooksinterface --> controllimitsearchresultslogbooksections
	thingresult --- controllimitresultsectionmore
	thingsection --- controllimitresultsectionmore
	thingsectioninterface --> controllimitresultsectionmore

@enduml
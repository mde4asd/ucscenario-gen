import React from 'react'

const URL_IMG="http://localhost:5000/uploads/out/"

const DiagramListItem = (props)=>  {
    const diagram=props.diagram;
    
    return (
    <li className="list-group-item" onClick={handleOnClick}>
        <div className="media">
            <div className="media-left">
                <img className="media-object img-rounded" height="100px" width="100px" src ={`${URL_IMG}${diagram.type}/${diagram.main_id}/${diagram.id}`}/>
            </div>
        

            <div className="media-body">
                <h5 className="title_list_item" >{diagram.title}</h5>
            </div>
        </div>
    </li>
    )
    function handleOnClick(){
    
        props.callback(diagram);
    }

}



export default DiagramListItem;
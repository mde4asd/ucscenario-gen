
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Security Requirements" as thingsecurityrequirements #grey
	entity "Datum" as thingdatum
	actor ", Staff\nMember" as actoritstaffmember
	boundary "Datum\nInterface" as thingdatuminterface #grey
	control "Know The\nSecurity Requirements\nOf The\nData" as controlknowsecurityrequirements

	thingdatum <.. thingsecurityrequirements
	actoritstaffmember --- thingdatuminterface
	thingsecurityrequirements --- controlknowsecurityrequirements
	thingdatum --- controlknowsecurityrequirements
	thingdatuminterface --> controlknowsecurityrequirements

@enduml

@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Follow Date" as thingfollowingdate
	entity "News Item" as thingnewsitem #grey
	entity "Date" as thingdate
	entity "News" as thingnews #grey
	actor "Site Editor" as actorsiteeditor
	boundary "Date\nInterface" as thingdateinterface #grey
	boundary "Item\nInterface" as thingiteminterface #grey
	control "Publishing Date\nOld News\nDate" as controlpublishdateoldnewsdate
	control "Set The\nFollowing Dates\nOn A\nNews Item" as controlsetfollowingdatenewsitem
	control "Publishing Date" as controlpublishdate

	thingnewsitem <.. thingfollowingdate
	thingnews <.. thingdate
	actorsiteeditor --- thingdateinterface
	actorsiteeditor --- thingiteminterface
	thingdate --- controlpublishdateoldnewsdate
	thingdateinterface --> controlpublishdateoldnewsdate
	thingfollowingdate --- controlsetfollowingdatenewsitem
	thingnewsitem --- controlsetfollowingdatenewsitem
	thingiteminterface --> controlsetfollowingdatenewsitem
	thingdate --- controlpublishdate
	thingdateinterface --> controlpublishdate

@enduml

@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Emergency" as thingemergency
	entity "Help" as thinghelp
	entity "Alfred" as thingALFRED
	actor "Olderperson" as actorOlderPerson
	circle "Case" as thingcase
	boundary "Alfred\nInterface" as thingalfredinterface #grey
	boundary "Emergency\nInterface" as thingemergencyinterface #grey
	control "Use Alfred" as controluseALFREDcontacthelpcase
	control "Contact Help\nIn Case\nOf An\nEmergency" as controlcontacthelpcase

	thingcase <.. thinghelp
	thingemergency *-- thingcase
	actorOlderPerson --- thingalfredinterface
	actorOlderPerson --- thingemergencyinterface
	thingALFRED --- controluseALFREDcontacthelpcase
	thingalfredinterface --> controluseALFREDcontacthelpcase
	controluseALFREDcontacthelpcase --> controlcontacthelpcase
	thinghelp --- controlcontacthelpcase
	thingcase --- controlcontacthelpcase
	thingemergency --- controlcontacthelpcase
	thingemergencyinterface --> controlcontacthelpcase

@enduml

@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Data Package\nProfile" as thingdatapackageprofile #grey
	actor "Researcherpublisher" as actorResearcherPublisher
	boundary "Datum" as thingdataResearcherPublisher
	control "Conforms To\nData Package\nProfile" as controlconformdataResearcherPublisherdatapackageprofile
	control "Know" as controlknowconformdataResearcherPublisherdatapackageprofile

	actorResearcherPublisher --- thingdataResearcherPublisher
	thingdataResearcherPublisher --> controlconformdataResearcherPublisherdatapackageprofile
	thingdatapackageprofile --- controlconformdataResearcherPublisherdatapackageprofile
	controlknowconformdataResearcherPublisherdatapackageprofile --> controlconformdataResearcherPublisherdatapackageprofile
	thingdataResearcherPublisher --> controlknowconformdataResearcherPublisherdatapackageprofile

@enduml
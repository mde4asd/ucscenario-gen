
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Price" as thingprice
	entity "Site" as thingsite
	actor "Publisher" as actorpublisher
	boundary "Price\nInterface" as thingpriceinterface #grey
	control "Know" as controlknow
	control "What The\nPrices Be" as controlbeprice

	actorpublisher --- thingpriceinterface
	thingprice --- controlbeprice
	thingpriceinterface --> controlbeprice

@enduml
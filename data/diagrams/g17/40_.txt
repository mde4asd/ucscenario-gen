
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Default" as thingdefault
	entity "Dataset Instance" as thingdatasetinstance #grey
	entity "Value" as thingvalue
	entity "Type" as thingtype
	entity "Property" as thingproperty
	entity "Dataset" as thingdataset #grey
	actor "User" as actoruser
	boundary "Instance\nInterface" as thinginstanceinterface #grey
	control "Find Out" as controlfind
	control "What The\nDefaults Be\nWhen Creating\nA Dataset\nInstance" as controlbedefaultdatasetinstance

	thingdatasetinstance <.. thingdefault
	thingdataset <.. thingtype
	actoruser --- thinginstanceinterface
	thingdefault --- controlbedefaultdatasetinstance
	thingdatasetinstance --- controlbedefaultdatasetinstance
	thinginstanceinterface --> controlbedefaultdatasetinstance

@enduml

@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Description" as thingdescription
	entity "Project Lifecycle" as thingprojectlifecycle #grey
	entity "Data Sets" as thingdatasets
	actor "Data Manager" as actordatamanager
	boundary "Description\nInterface" as thingdescriptioninterface #grey
	boundary "Sets\nInterface" as thingsetsinterface #grey
	control "Update Throughout\nThe Project\nLifecycle" as controlupdateprojectlifecycle
	control "Have The\nDescription Of\nCollected Data\nSets Used" as controlhavedescription
	control "Have The\nDescription Of\nCollected Sets" as controlhavedescriptionupdateprojectlifecycle

	thingdatasets *-- thingdescription
	actordatamanager --- thingdescriptioninterface
	actordatamanager --- thingsetsinterface
	thingprojectlifecycle --- controlupdateprojectlifecycle
	controlhavedescriptionupdateprojectlifecycle --> controlupdateprojectlifecycle
	thingdescriptioninterface --> controlupdateprojectlifecycle
	thingdescription --- controlhavedescription
	thingdatasets --- controlhavedescription
	thingsetsinterface --> controlhavedescription
	thingdescriptioninterface --> controlhavedescriptionupdateprojectlifecycle
	thingdescription --- controlhavedescriptionupdateprojectlifecycle

@enduml
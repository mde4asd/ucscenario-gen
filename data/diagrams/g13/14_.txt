
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Spreadsheet" as thingspreadsheet
	entity "Story" as thingstory
	actor "Moderator" as actormoderator
	boundary "Spreadsheet\nInterface" as thingspreadsheetinterface #grey
	control "Copy Stories\nFrom A\nSpreadsheet" as controlcopystoryspreadsheet
	control "Paste Stories\nFrom A\nSpreadsheet" as controlpastestory

	thingspreadsheet <.. thingstory
	actormoderator --- thingspreadsheetinterface
	thingstory --- controlcopystoryspreadsheet
	thingspreadsheet --- controlcopystoryspreadsheet
	thingspreadsheetinterface --> controlcopystoryspreadsheet
	thingspreadsheet --- controlpastestory
	thingstory --- controlpastestory
	thingspreadsheetinterface --> controlpastestory

@enduml
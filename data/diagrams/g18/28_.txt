
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "File" as thingfile
	actor "User" as actoruser
	boundary "File\nInterface" as thingfileinterface #grey
	control "Have Files" as controlhavefile
	control "Delete Restorable" as controldeletehavefile

	actoruser --- thingfileinterface
	thingfile --- controlhavefile
	controldeletehavefile --> controlhavefile
	thingfileinterface --> controlhavefile
	thingfileinterface --> controldeletehavefile

@enduml
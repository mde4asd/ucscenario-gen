
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Payment Page" as thingpaymentpage #grey
	entity "Registration Process" as thingregistrationprocess #grey
	entity "Individual Sponsorship" as thingindividualsponsorship
	actor "Authenticateduser" as actorauthenticateduser
	circle "Step" as thingthirdstep
	boundary "Page\nInterface" as thingpageinterface #grey
	control "Pay For\nIndividual Sponsorships\nAs The\nThird Step\nOf The\nRegistration Process" as controlpayindividualsponsorshipthirdstep
	control "See A\nPayment Page" as controlseepaymentpagepayindividualsponsorshipthirdstep

	thingthirdstep <.. thingindividualsponsorship
	thingregistrationprocess *-- thingthirdstep
	actorauthenticateduser --- thingpageinterface
	thingindividualsponsorship --- controlpayindividualsponsorshipthirdstep
	thingthirdstep --- controlpayindividualsponsorshipthirdstep
	thingregistrationprocess --- controlpayindividualsponsorshipthirdstep
	controlseepaymentpagepayindividualsponsorshipthirdstep --> controlpayindividualsponsorshipthirdstep
	thingpageinterface --> controlpayindividualsponsorshipthirdstep
	thingpaymentpage --- controlseepaymentpagepayindividualsponsorshipthirdstep
	thingpageinterface --> controlseepaymentpagepayindividualsponsorshipthirdstep

@enduml
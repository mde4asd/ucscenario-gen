
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Specific Button" as thingspecificbutton
	entity "Alfred" as thingALFRED
	entity "Interaction Mode" as thinginteractionmode
	actor "Olderperson" as actorOlderPerson
	boundary "Button\nInterface" as thingbuttoninterface #grey
	control "Activate The\nVocal Interaction\nMode Of\nAlfred Pushing\nA Specific\nButton" as controlactivatevocalinteractionmodespecificbutton

	thingspecificbutton <.. thingALFRED
	thingALFRED <.. thinginteractionmode
	actorOlderPerson --- thingbuttoninterface
	thinginteractionmode --- controlactivatevocalinteractionmodespecificbutton
	thingALFRED --- controlactivatevocalinteractionmodespecificbutton
	thingspecificbutton --- controlactivatevocalinteractionmodespecificbutton
	thingbuttoninterface --> controlactivatevocalinteractionmodespecificbutton

@enduml
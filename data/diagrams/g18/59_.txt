
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Neurohub" as thingneurohub
	entity "Student" as thingstudent
	entity "Feedback" as thingfeedback
	entity "Form" as thingform #grey
	entity "Course" as thingcourse
	actor "Supervisor" as actorsupervisor
	circle "Forms" as thingforms #grey
	boundary "Order" as thingorderprovideformfeedbacksupervisor #grey
	boundary "Neurohub\nInterface" as thingneurohubinterface #grey
	control "Create Feedback\nForms Within\nNeurohub" as controlcreatefeedbackformsneurohub
	control "Forms Provide\nWith Feedback\nAbout The\nCourse That\nAttended" as controlprovideformfeedback

	thingcourse <.. thingfeedback
	thingneurohub *-- thingforms
	thingfeedback *-- thingforms
	actorsupervisor --- thingorderprovideformfeedbacksupervisor
	actorsupervisor --- thingneurohubinterface
	thingforms --- controlcreatefeedbackformsneurohub
	thingneurohub --- controlcreatefeedbackformsneurohub
	thingneurohubinterface --> controlcreatefeedbackformsneurohub
	thingorderprovideformfeedbacksupervisor --> controlprovideformfeedback
	thingform --- controlprovideformfeedback
	thingfeedback --- controlprovideformfeedback
	thingcourse --- controlprovideformfeedback

@enduml

@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Material Types" as thingmaterialtypes
	actor "Archivist" as actorarchivist
	boundary "Types\nInterface" as thingtypesinterface #grey
	control "Search Individual\nMaterial Types" as controlsearchindividualmaterialtypes

	actorarchivist --- thingtypesinterface
	thingmaterialtypes --- controlsearchindividualmaterialtypes
	thingtypesinterface --> controlsearchindividualmaterialtypes

@enduml
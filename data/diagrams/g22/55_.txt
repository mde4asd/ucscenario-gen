
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Datum" as thingdatum
	actor "Archivemanager" as actorarchivemanager
	boundary "Detailed Metadata" as thingdetailedmetadataarchivemanager
	control "Associated To\nData" as controlassociatedetailedmetadataarchivemanagerdatum
	control "Make" as controlmakeassociatedetailedmetadataarchivemanagerdatum

	actorarchivemanager --- thingdetailedmetadataarchivemanager
	thingdetailedmetadataarchivemanager --> controlassociatedetailedmetadataarchivemanagerdatum
	thingdatum --- controlassociatedetailedmetadataarchivemanagerdatum
	controlmakeassociatedetailedmetadataarchivemanagerdatum --> controlassociatedetailedmetadataarchivemanagerdatum
	thingdetailedmetadataarchivemanager --> controlmakeassociatedetailedmetadataarchivemanagerdatum

@enduml

@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Repository" as thingrepository
	entity "Collection" as thingcollection
	actor "Faculty Member" as actorfacultymember
	boundary "Repository\nInterface" as thingrepositoryinterface #grey
	control "Access A\nCollection Within\nThe Repository" as controlaccesscollection

	thingrepository <.. thingcollection
	actorfacultymember --- thingrepositoryinterface
	thingcollection --- controlaccesscollection
	thingrepository --- controlaccesscollection
	thingrepositoryinterface --> controlaccesscollection

@enduml
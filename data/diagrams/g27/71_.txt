
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Metadata" as thingmetadata
	entity "System" as thingothersystem
	entity "Content" as thingcontent
	entity "Repository" as thingrepository
	entity "Web Page" as thingwebpage #grey
	actor "Library ,\nStaff Member" as actorlibrarystaffmember
	boundary "Metadata\nInterface" as thingmetadatainterface #grey
	boundary "Page\nInterface" as thingpageinterface #grey
	boundary "System\nInterface" as thingsysteminterface #grey
	control "Use Metadata" as controlusemetadata
	control "Use Content\nFrom Web\nPage" as controlusecontentwebpage
	control "Use Content\nFrom The\nRepository In\nOther Systems" as controlusecontentrepository

	thingrepository <.. thingcontent
	thingwebpage <.. thingcontent
	thingothersystem <.. thingrepository
	actorlibrarystaffmember --- thingmetadatainterface
	actorlibrarystaffmember --- thingpageinterface
	actorlibrarystaffmember --- thingsysteminterface
	thingmetadata --- controlusemetadata
	thingmetadatainterface --> controlusemetadata
	thingwebpage --- controlusecontentwebpage
	thingpageinterface --> controlusecontentwebpage
	thingcontent --- controlusecontentwebpage
	thingcontent --- controlusecontentrepository
	thingrepository --- controlusecontentrepository
	thingothersystem --- controlusecontentrepository
	thingsysteminterface --> controlusecontentrepository

@enduml
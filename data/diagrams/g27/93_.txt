
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Custom Metadata\nFields" as thingcustommetadatafields #grey
	entity "Certain Collection" as thingcertaincollection
	entity "Field" as thingfield
	actor "Library ,\nStaff Member" as actorlibrarystaffmember
	circle "Searching" as thingsearching
	boundary "Collection\nInterface" as thingcollectioninterface #grey
	boundary "Field\nInterface" as thingfieldinterface #grey
	control "Create Custom\nMetadata Fields\nFor Certain\nCollections" as controlcreatecustommetadatafields
	control "Allow Searching\nOf Those\nFields" as controlallowsearching

	thingcertaincollection <.. thingcustommetadatafields
	thingfield *-- thingsearching
	actorlibrarystaffmember --- thingcollectioninterface
	actorlibrarystaffmember --- thingfieldinterface
	thingcustommetadatafields --- controlcreatecustommetadatafields
	thingcertaincollection --- controlcreatecustommetadatafields
	thingcollectioninterface --> controlcreatecustommetadatafields
	thingsearching --- controlallowsearching
	thingfield --- controlallowsearching
	thingfieldinterface --> controlallowsearching

@enduml
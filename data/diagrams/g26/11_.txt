
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Statement" as thingstatement
	entity "Rights Statuses" as thingrightsstatuses
	actor "Archivist" as actorarchivist
	boundary "Statement\nInterface" as thingstatementinterface #grey
	boundary "Statuses\nInterface" as thingstatusesinterface #grey
	control "Select Statement" as controlselectstatement
	control "Select Existing\nRights Statuses" as controlselectexistingrightsstatuses

	actorarchivist --- thingstatementinterface
	actorarchivist --- thingstatusesinterface
	thingstatement --- controlselectstatement
	thingstatementinterface --> controlselectstatement
	thingrightsstatuses --- controlselectexistingrightsstatuses
	thingstatusesinterface --> controlselectexistingrightsstatuses

@enduml
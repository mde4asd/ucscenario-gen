
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Field" as thingfield
	actor "Broker User" as actorbrokeruser
	boundary "Historical Fabs\nLoader" as thinghistoricalfabsloaderderivefieldbrokeruser
	control "Derive Fields" as controlderivefield

	actorbrokeruser --- thinghistoricalfabsloaderderivefieldbrokeruser
	thinghistoricalfabsloaderderivefieldbrokeruser --> controlderivefield
	thingfield --- controlderivefield

@enduml
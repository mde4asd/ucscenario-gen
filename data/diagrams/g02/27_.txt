
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Fabs Sample\nFile" as thingfabssamplefile #grey
	actor "Developer" as actordeveloper
	boundary "File\nInterface" as thingfileinterface #grey
	control "Update The\nFabs Sample\nFile" as controlupdatefabssamplefileremove
	control "Remove" as controlremove

	actordeveloper --- thingfileinterface
	thingfabssamplefile --- controlupdatefabssamplefileremove
	thingfileinterface --> controlupdatefabssamplefileremove
	controlupdatefabssamplefileremove --> controlremove
	thingfileinterface --> controlremove

@enduml
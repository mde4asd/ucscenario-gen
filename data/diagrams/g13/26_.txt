
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Team" as thingteam
	entity "0" as thing0
	entity "1 ,\n4 ,\n8 ,\n16 ,\n32 ,\nEtc 2" as thing1481632etc2
	entity "8" as thing8
	entity "1" as thing1
	entity "3" as thing3
	entity "2" as thing2
	entity "Etc" as thingetc
	entity "5" as thing5
	actor "Moderator" as actormoderator
	boundary "1\nInterface" as thing1interface #grey
	boundary "8\nInterface" as thing8interface #grey
	boundary "2\nInterface" as thing2interface #grey
	boundary "3\nInterface" as thing3interface #grey
	boundary "Etc\nInterface" as thingetcinterface #grey
	boundary "5\nInterface" as thing5interface #grey
	control "Have" as controlhave
	control "Select The\nTeam Estimate\nWith 0\n1" as controlselecthaveteam01
	control "Select The\nTeam Estimate\nWith 0\n8" as controlselecthaveteam08
	control "Select The\nTeam Estimate\nWith 0\n2" as controlselecthaveteam02
	control "Select The\nTeam Estimate\nWith 0\n3" as controlselecthaveteam03
	control "Select The\nTeam Estimate\nWith 0\nEtc" as controlselecthaveteam0etc
	control "Select The\nTeam Estimate\nWith 0\n5" as controlselecthaveteam05

	thing0 <.. thingteam
	thing1 <.. thing0
	thing5 <.. thing0
	thingetc <.. thing0
	thing8 <.. thing0
	thing3 <.. thing0
	thing2 <.. thing0
	actormoderator --- thing1interface
	actormoderator --- thing8interface
	actormoderator --- thing2interface
	actormoderator --- thing3interface
	actormoderator --- thingetcinterface
	actormoderator --- thing5interface
	controlselecthaveteam01 --> controlhave
	thing1interface --> controlhave
	controlselecthaveteam05 --> controlhave
	controlselecthaveteam02 --> controlhave
	controlselecthaveteam08 --> controlhave
	controlselecthaveteam03 --> controlhave
	controlselecthaveteam0etc --> controlhave
	thingteam --- controlselecthaveteam01
	thing0 --- controlselecthaveteam01
	thing1 --- controlselecthaveteam01
	thing1interface --> controlselecthaveteam01
	thing8 --- controlselecthaveteam08
	thing8interface --> controlselecthaveteam08
	thingteam --- controlselecthaveteam08
	thing0 --- controlselecthaveteam08
	thing2interface --> controlselecthaveteam02
	thing2 --- controlselecthaveteam02
	thingteam --- controlselecthaveteam02
	thing0 --- controlselecthaveteam02
	thing3 --- controlselecthaveteam03
	thing3interface --> controlselecthaveteam03
	thingteam --- controlselecthaveteam03
	thing0 --- controlselecthaveteam03
	thingetc --- controlselecthaveteam0etc
	thingetcinterface --> controlselecthaveteam0etc
	thingteam --- controlselecthaveteam0etc
	thing0 --- controlselecthaveteam0etc
	thing5 --- controlselecthaveteam05
	thing5interface --> controlselecthaveteam05
	thingteam --- controlselecthaveteam05
	thing0 --- controlselecthaveteam05

@enduml
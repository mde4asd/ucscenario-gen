from enum import Enum, auto
from typing import List


class EnumStr(Enum):

    def __str__(self):
        return str(self.__class__.__name__) + "." + str(self.name)

    @classmethod
    def from_str(cls: Enum, string: str) -> super:
        """
        Safer method than calling enum constructor directly. Accounts for some special characters.
        :param string: The string value to convert to an enum value
        :return: The enum value corresponding to the string.
        """
        string = string.upper()
        string.replace("$", "_dollar")
        string.replace("-", "")
        if string == ",":
            string = "COMMA"
        if string == ".":
            string = "STOP"
        if string in [":", "''", "\"\"", "#", "``", "$"]:
            string = "PUNCT"
        for key, val in cls.__dict__.items():
            if key.upper() == string:
                return cls(val)


class Arc(EnumStr):
    acomp = auto()
    acl = auto()
    advcl = auto()
    advmod = auto()
    agent = auto()
    amod = auto()
    appos = auto()
    attr = auto()
    aux = auto()
    auxpass = auto()
    case = auto()
    cc = auto()
    ccomp = auto()
    complm = auto()
    compound = auto()
    conj = auto()
    cop = auto()
    csubj = auto()
    csubjpass = auto()
    dative = auto()
    dep = auto()
    det = auto()
    dobj = auto()
    expl = auto()
    hmod = auto()
    hyph = auto()
    infmod = auto()
    intj = auto()
    iobj = auto()
    mark = auto()
    meta = auto()
    neg = auto()
    nmod = auto()
    nn = auto()
    npadvmod = auto()
    nsubj = auto()
    nsubjpass = auto()
    num = auto()
    number = auto()
    nummod = auto()
    oprd = auto()
    obj = auto()
    obl = auto()
    parataxis = auto()
    partmod = auto()
    pcomp = auto()
    pobj = auto()
    poss = auto()
    possessive = auto()
    preconj = auto()
    prep = auto()
    prt = auto()
    punct = auto()
    quantmod = auto()
    rcmod = auto()
    relcl = auto()
    root = auto()
    ROOT = auto()
    xcomp = auto()

    @staticmethod
    def all_obj() -> List["Arc"]:
        return [Arc.dobj, Arc.pobj, Arc.obj, Arc.iobj]

    @staticmethod
    def all_subj() -> List["Arc"]:
        return [Arc.nsubj, Arc.csubj, Arc.csubjpass, Arc.nsubjpass]

    @staticmethod
    def active_subj() -> List["Arc"]:
        return [Arc.csubj, Arc.nsubj]

    @staticmethod
    def passive_subj() -> List["Arc"]:
        return [Arc.nsubjpass, Arc.csubjpass]

    @staticmethod
    def all_mod() -> List["Arc"]:
        return [Arc.amod, Arc.nmod, Arc.advmod, Arc.quantmod, Arc.rcmod, Arc.partmod, Arc.npadvmod, Arc.infmod,
                Arc.hmod]

    @staticmethod
    def all_comp() -> List["Arc"]:
        return [Arc.acomp, Arc.xcomp, Arc.ccomp, Arc.pcomp]

    @staticmethod
    def all_context() -> List["Arc"]:
        return [Arc.det, Arc.aux]

    @staticmethod
    def all_clause() -> List["Arc"]:
        return [Arc.relcl, Arc.advcl, Arc.acl]


class POSFine(EnumStr):
    LRB = auto()
    RRB = auto()
    PUNCT = auto()
    ADD = auto()
    AFX = auto()
    BES = auto()
    CC = auto()
    COMMA = auto()
    DD = auto()
    DT = auto()
    EX = auto()
    FW = auto()
    GW = auto()
    HVS = auto()
    HYPH = auto()
    IN = auto()
    JJ = auto()
    JJR = auto()
    JJS = auto()
    LS = auto()
    MD = auto()
    NFP = auto()
    NIL = auto()
    NN = auto()
    NNP = auto()
    NNPS = auto()
    NNS = auto()
    PDT = auto()
    POS = auto()
    PRP = auto()
    PRP_dollar = auto()
    RB = auto()
    RBR = auto()
    RBS = auto()
    RP = auto()
    _SP = auto()
    STOP = auto()
    SYM = auto()
    TO = auto()
    UH = auto()
    VB = auto()
    VBN = auto()
    VBP = auto()
    VBZ = auto()
    VBD = auto()
    VBG = auto()
    WDT = auto()
    WP = auto()
    WP_dollar = auto()
    WRB = auto()
    XX = auto()

    @staticmethod
    def all_noun() -> List["POSFine"]:
        return [POSFine.NN, POSFine.NNS, POSFine.NNP, POSFine.NNPS]
    
    @staticmethod
    def all_verb() -> List["POSFine"]:
        return [POSFine.VB, POSFine.VBN, POSFine.VBP, POSFine.VBZ, POSFine.VBD, POSFine.VBG]


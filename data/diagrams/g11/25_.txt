
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Webmaster Report" as thingwebmasterreport
	actor "Cms Sme" as actorcmssme
	boundary "Report\nInterface" as thingreportinterface #grey
	control "See The\nWorkflow Webmaster\nReport" as controlseeworkflowwebmasterreport

	actorcmssme --- thingreportinterface
	thingwebmasterreport --- controlseeworkflowwebmasterreport
	thingreportinterface --> controlseeworkflowwebmasterreport

@enduml

@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "App" as thingapp
	entity "Data Visualisations" as thingdatavisualisations
	actor "Openspending ,\nCommunity Member" as actoropenspendingcommunitymember
	circle "Example" as thingexample
	circle "Use" as thinguse
	boundary "Visualisations\nInterface" as thingvisualisationsinterface #grey
	control "Have An\nApp" as controlhaveapp
	control "Find Examples\nOf Use\nOf Fiscal\nData Visualisations" as controlfindhaveappexample

	thinguse *-- thingexample
	thingdatavisualisations *-- thinguse
	actoropenspendingcommunitymember --- thingvisualisationsinterface
	thingapp --- controlhaveapp
	controlfindhaveappexample --> controlhaveapp
	thingvisualisationsinterface --> controlhaveapp
	thingexample --- controlfindhaveappexample
	thinguse --- controlfindhaveappexample
	thingdatavisualisations --- controlfindhaveappexample
	thingvisualisationsinterface --> controlfindhaveappexample

@enduml

@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Ingest ,\nSimple Format" as thingingestsimpleformat
	entity "Component" as thingcomponent
	entity "New Version" as thingnewversion
	entity "Exist Item" as thingexistingitem
	actor "Datum Depositor" as actordatadepositor
	boundary "Version\nInterface" as thingversioninterface #grey
	boundary "Format\nInterface" as thingformatinterface #grey
	control "Create New\nVersions" as controlcreatenewversioncreatenewone
	control "Update Existing\nItems" as controlupdateexistingitem
	control "Create New\nVersions Where\nAlready Exist" as controlcreatenewversion
	control "Update Component" as controlupdatecomponentcomponent
	control "Use The\nIngest, Simple\nFormat" as controluseingestsimpleformatupdateupdatecomponentcomponent
	control "Create New\nOnes Where\nThen'T Do" as controlcreatenewone

	actordatadepositor --- thingversioninterface
	actordatadepositor --- thingformatinterface
	thingversioninterface --> controlcreatenewversioncreatenewone
	thingnewversion --- controlcreatenewversioncreatenewone
	thingexistingitem --- controlupdateexistingitem
	thingformatinterface --> controlupdateexistingitem
	controluseingestsimpleformatupdateupdatecomponentcomponent --> controlupdateexistingitem
	thingnewversion --- controlcreatenewversion
	thingversioninterface --> controlcreatenewversion
	controlupdatecomponentcomponent --> controlcreatenewversion
	controluseingestsimpleformatupdateupdatecomponentcomponent --> controlupdatecomponentcomponent
	thingcomponent --- controlupdatecomponentcomponent
	thingformatinterface --> controlupdatecomponentcomponent
	thingingestsimpleformat --- controluseingestsimpleformatupdateupdatecomponentcomponent
	thingformatinterface --> controluseingestsimpleformatupdateupdatecomponentcomponent
	controlcreatenewversioncreatenewone --> controlcreatenewone
	thingversioninterface --> controlcreatenewone

@enduml
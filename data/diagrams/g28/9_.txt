
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Similar Profile" as thingsimilarprofile
	entity "Content Recommendations" as thingcontentrecommendations #grey
	actor "User" as actoruser
	boundary "Profile\nInterface" as thingprofileinterface #grey
	control "Receive Content\nRecommendations From\nSimilar Profiles" as controlreceivecontentrecommendations

	thingsimilarprofile <.. thingcontentrecommendations
	actoruser --- thingprofileinterface
	thingcontentrecommendations --- controlreceivecontentrecommendations
	thingsimilarprofile --- controlreceivecontentrecommendations
	thingprofileinterface --> controlreceivecontentrecommendations

@enduml

@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Record" as thingrecord
	entity "Data Complete" as thingdatacomplete
	actor "Research ,\nInformation Manager" as actorresearchinformationmanager
	boundary "Complete\nInterface" as thingcompleteinterface #grey
	control "Include Records\nFor Externally\nHeld Data\nComplete" as controlincluderecord

	thingdatacomplete <.. thingrecord
	actorresearchinformationmanager --- thingcompleteinterface
	thingrecord --- controlincluderecord
	thingdatacomplete --- controlincluderecord
	thingcompleteinterface --> controlincluderecord

@enduml
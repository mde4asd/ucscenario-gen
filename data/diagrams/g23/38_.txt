
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Rights Management\nStatements" as thingrightsmanagementstatements #grey
	actor "Archivist" as actorarchivist
	boundary "Statements\nInterface" as thingstatementsinterface #grey
	control "Create Rights\nManagement Statements" as controlcreaterightsmanagementstatements
	control "Edit Rights\nManagement Statements" as controleditrightsmanagementstatements

	actorarchivist --- thingstatementsinterface
	thingrightsmanagementstatements --- controlcreaterightsmanagementstatements
	thingstatementsinterface --> controlcreaterightsmanagementstatements
	thingrightsmanagementstatements --- controleditrightsmanagementstatements
	thingstatementsinterface --> controleditrightsmanagementstatements

@enduml
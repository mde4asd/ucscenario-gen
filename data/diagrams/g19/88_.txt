
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Heart Rate" as thingheartrate #grey
	entity "User" as thinguser
	entity "Alfred" as thingALFRED
	actor "Medicalcaregiver" as actorMedicalCaregiver
	boundary "Alfred\nInterface" as thingalfredinterface #grey
	boundary "Rate\nInterface" as thingrateinterface #grey
	control "Have To\nAlfred" as controlhaveALFRED
	control "Determine The\nUser's Heart\nRate" as controldetermineheartrate

	actorMedicalCaregiver --- thingalfredinterface
	actorMedicalCaregiver --- thingrateinterface
	thingALFRED --- controlhaveALFRED
	thingalfredinterface --> controlhaveALFRED
	thingheartrate --- controldetermineheartrate
	thingrateinterface --> controldetermineheartrate

@enduml
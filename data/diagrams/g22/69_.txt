
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "External Repository" as thingexternalrepository
	entity "Data Sets" as thingdatasets
	actor "Institutional ,\nData Steward" as actorinstitutionaldatasteward
	boundary "Repository\nInterface" as thingrepositoryinterface #grey
	control "Extract The\nData Sets\nSubmitted To\nExternal Repositories" as controlextractdatasets

	thingexternalrepository <.. thingdatasets
	actorinstitutionaldatasteward --- thingrepositoryinterface
	thingdatasets --- controlextractdatasets
	thingexternalrepository --- controlextractdatasets
	thingrepositoryinterface --> controlextractdatasets

@enduml
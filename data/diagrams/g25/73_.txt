
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Occupation" as thingoccupation
	entity "Function" as thingfunction
	entity "Subject" as thingsubject
	entity "Cartographic" as thingcartographic
	entity "Topic" as thingtopic
	entity "Collection" as thingcollection
	entity "Etc" as thingetc
	entity "Form" as thingform
	entity "Title" as thingtitle
	entity "Genre" as thinggenre
	entity "Object" as thingobject
	actor "Repository Manager" as actorrepositorymanager
	circle "Component" as thingcomponent #grey
	boundary "Object\nInterface" as thingobjectinterface #grey
	boundary "Function\nInterface" as thingfunctioninterface #grey
	boundary "Collection\nInterface" as thingcollectioninterface #grey
	boundary "Topic\nInterface" as thingtopicinterface #grey
	boundary "Form\nInterface" as thingforminterface #grey
	boundary "Genre\nInterface" as thinggenreinterface #grey
	boundary "Occupation\nInterface" as thingoccupationinterface #grey
	boundary "Cartographic\nInterface" as thingcartographicinterface #grey
	boundary "Etc\nInterface" as thingetcinterface #grey
	boundary "Title\nInterface" as thingtitleinterface #grey
	control "Indicate All\nSubjects Pertinent\nTo Object" as controlindicatesubjectobject
	control "Indicate Function" as controlindicatefunction
	control "Indicate All\nSubjects Pertinent\nTo Object\nComponent" as controlindicatesubjectobjectcomponent
	control "Indicate Geographical\nName" as controlindicategeographicalname
	control "Indicate All\nSubjects Pertinent\nTo A\nCollection" as controlindicatesubjectcollection
	control "Indicate Topic" as controlindicatetopic
	control "Indicate Form" as controlindicateform
	control "Indicate Genre" as controlindicategenre
	control "Indicate Occupation" as controlindicateoccupation
	control "Indicate Cartographic" as controlindicatecartographic
	control "Indicate Name" as controlindicatename
	control "Indicate Etc" as controlindicateetc
	control "Indicate Title" as controlindicatetitle

	thingcollection <.. thingsubject
	thingobject <.. thingsubject
	thingcomponent <.. thingsubject
	thingobject *-- thingcomponent
	actorrepositorymanager --- thingobjectinterface
	actorrepositorymanager --- thingfunctioninterface
	actorrepositorymanager --- thingcollectioninterface
	actorrepositorymanager --- thingtopicinterface
	actorrepositorymanager --- thingforminterface
	actorrepositorymanager --- thinggenreinterface
	actorrepositorymanager --- thingoccupationinterface
	actorrepositorymanager --- thingcartographicinterface
	actorrepositorymanager --- thingetcinterface
	actorrepositorymanager --- thingtitleinterface
	thingobject --- controlindicatesubjectobject
	thingobjectinterface --> controlindicatesubjectobject
	thingsubject --- controlindicatesubjectobject
	thingfunction --- controlindicatefunction
	thingfunctioninterface --> controlindicatefunction
	thingcomponent --- controlindicatesubjectobjectcomponent
	thingobjectinterface --> controlindicatesubjectobjectcomponent
	thingsubject --- controlindicatesubjectobjectcomponent
	thingsubject --- controlindicatesubjectcollection
	thingcollection --- controlindicatesubjectcollection
	thingcollectioninterface --> controlindicatesubjectcollection
	thingtopic --- controlindicatetopic
	thingtopicinterface --> controlindicatetopic
	thingform --- controlindicateform
	thingforminterface --> controlindicateform
	thinggenre --- controlindicategenre
	thinggenreinterface --> controlindicategenre
	thingoccupation --- controlindicateoccupation
	thingoccupationinterface --> controlindicateoccupation
	thingcartographic --- controlindicatecartographic
	thingcartographicinterface --> controlindicatecartographic
	thingetc --- controlindicateetc
	thingetcinterface --> controlindicateetc
	thingtitle --- controlindicatetitle
	thingtitleinterface --> controlindicatetitle

@enduml

@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Object" as thingobject
	entity "Scope" as thingscope
	entity "Collection" as thingcollection
	entity "Identifier" as thingvariousidentifier
	entity "Content" as thingcontent
	entity "Use" as thinguse
	entity "Access" as thingaccess
	actor "User" as actoruser
	circle "Facet" as thingvariousfacet
	circle "Component" as thingcomponent #grey
	boundary "Object\nInterface" as thingobjectinterface #grey
	boundary "Use\nInterface" as thinguseinterface #grey
	boundary "Scope\nInterface" as thingscopeinterface #grey
	boundary "Identifier\nInterface" as thingidentifierinterface #grey
	boundary "Content\nInterface" as thingcontentinterface #grey
	boundary "Access\nInterface" as thingaccessinterface #grey
	control "Know About\nVarious Facets\nOf Object" as controlknowvariousfacetobject
	control "Govern Use" as controlgovernuse
	control "Know About\nVarious Facets\nOf A\nCollection Such\nAs Scope" as controlknowvariousfacet
	control "Know About\nVarious Facets\nOf Object\nComponent" as controlknowvariousfacetobjectcomponent
	control "Know About\nVarious Facets\nOf A\nCollection Such\nAs Various\nIdentifier" as controlknowvariousfacetvariousidentifier
	control "Know About\nVarious Facets\nOf A\nCollection Such\nAs Content" as controlknowvariousfacetcontent
	control "Govern Access" as controlgovernaccess

	thingscope <.. thingcollection
	thingcontent <.. thingcollection
	thingvariousidentifier <.. thingcollection
	thingcollection *-- thingvariousfacet
	thingobject *-- thingvariousfacet
	thingcomponent *-- thingvariousfacet
	thingobject *-- thingcomponent
	actoruser --- thingobjectinterface
	actoruser --- thinguseinterface
	actoruser --- thingscopeinterface
	actoruser --- thingidentifierinterface
	actoruser --- thingcontentinterface
	actoruser --- thingaccessinterface
	thingobject --- controlknowvariousfacetobject
	thingobjectinterface --> controlknowvariousfacetobject
	thingvariousfacet --- controlknowvariousfacetobject
	thinguse --- controlgovernuse
	thinguseinterface --> controlgovernuse
	thingvariousfacet --- controlknowvariousfacet
	thingcollection --- controlknowvariousfacet
	thingscope --- controlknowvariousfacet
	thingscopeinterface --> controlknowvariousfacet
	thingcomponent --- controlknowvariousfacetobjectcomponent
	thingvariousfacet --- controlknowvariousfacetobjectcomponent
	thingobjectinterface --> controlknowvariousfacetobjectcomponent
	thingvariousidentifier --- controlknowvariousfacetvariousidentifier
	thingidentifierinterface --> controlknowvariousfacetvariousidentifier
	thingvariousfacet --- controlknowvariousfacetvariousidentifier
	thingcollection --- controlknowvariousfacetvariousidentifier
	thingcontent --- controlknowvariousfacetcontent
	thingcontentinterface --> controlknowvariousfacetcontent
	thingvariousfacet --- controlknowvariousfacetcontent
	thingcollection --- controlknowvariousfacetcontent
	thingaccess --- controlgovernaccess
	thingaccessinterface --> controlgovernaccess

@enduml
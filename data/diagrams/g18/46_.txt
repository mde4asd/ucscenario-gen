
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Search" as thingsearch
	entity "Log Books" as thinglogbooks #grey
	actor "User" as actoruser
	boundary "Books\nInterface" as thingbooksinterface #grey
	control "Limit Search\nTo A\nParticular Set\nOf Log\nBooks" as controllimitsearchparticularset

	thinglogbooks <.. thingsearch
	actoruser --- thingbooksinterface
	thingsearch --- controllimitsearchparticularset
	thinglogbooks --- controllimitsearchparticularset
	thingbooksinterface --> controllimitsearchparticularset

@enduml
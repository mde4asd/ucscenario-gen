
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Integrity" as thingintegrity
	entity "Manager" as thingmanager #grey
	entity "Question" as thingquestion
	entity "Object" as thingobject
	entity "Preservation Managers" as thingpreservationmanagers #grey
	entity "Authenticity" as thingauthenticity
	entity "File Format" as thingfileformat
	actor "Dams Manager" as actordamsmanager
	boundary "Object\nInterface" as thingobjectinterface #grey
	boundary "Authenticity\nInterface" as thingauthenticityinterface #grey
	boundary "Format\nInterface" as thingformatinterface #grey
	boundary "Question\nInterface" as thingquestioninterface #grey
	control "Know Via\nManagers Of\nAny Object" as controlknowmanagerobjectupdatecurrentfileformat
	control "Know Via\nManagers Of\nAny Object\nFor Which\nAuthenticity Is" as controlknowmanagerobjectauthenticity
	control "Update To\nA More\nCurrent File\nFormat" as controlupdatecurrentfileformat
	control "Know Via\nPreservation Managers\nOf Any\nObject For\nWhich Integrity\nIs In\nQuestion" as controlknowpreservationmanagersobject

	thingquestion <.. thingintegrity
	thingobject <.. thingmanager
	thingintegrity <.. thingobject
	thingquestion <.. thingobject
	thingauthenticity <.. thingobject
	thingobject <.. thingpreservationmanagers
	actordamsmanager --- thingobjectinterface
	actordamsmanager --- thingauthenticityinterface
	actordamsmanager --- thingformatinterface
	actordamsmanager --- thingquestioninterface
	thingmanager --- controlknowmanagerobjectupdatecurrentfileformat
	thingobjectinterface --> controlknowmanagerobjectupdatecurrentfileformat
	thingobject --- controlknowmanagerobjectupdatecurrentfileformat
	thingauthenticity --- controlknowmanagerobjectauthenticity
	thingauthenticityinterface --> controlknowmanagerobjectauthenticity
	thingmanager --- controlknowmanagerobjectauthenticity
	thingobject --- controlknowmanagerobjectauthenticity
	controlknowmanagerobjectupdatecurrentfileformat --> controlupdatecurrentfileformat
	thingfileformat --- controlupdatecurrentfileformat
	thingformatinterface --> controlupdatecurrentfileformat
	thingpreservationmanagers --- controlknowpreservationmanagersobject
	thingobject --- controlknowpreservationmanagersobject
	thingintegrity --- controlknowpreservationmanagersobject
	thingquestion --- controlknowpreservationmanagersobject
	thingquestioninterface --> controlknowpreservationmanagersobject

@enduml

@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "News Items" as thingnewsitems #grey
	entity "Editor" as thingeditor
	actor "Site Visitor" as actorsitevisitor
	boundary "Editor\nInterface" as thingeditorinterface #grey
	control "Email News\nItems To\nThe Editor" as controlemailnewsitemseditor

	thingeditor <.. thingnewsitems
	actorsitevisitor --- thingeditorinterface
	thingnewsitems --- controlemailnewsitemseditor
	thingeditor --- controlemailnewsitemseditor
	thingeditorinterface --> controlemailnewsitemseditor

@enduml

@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Artefact" as thingartefact
	entity "Prefiltering" as thingprefiltering
	entity "Camera" as thingcamera
	entity "Image" as thingimage
	entity "Error" as thingerror
	entity "Bright Star" as thingbrightstar
	actor "Zooniverse Admin" as actorzooniverseadmin
	boundary "Image\nInterface" as thingimageinterface #grey
	boundary "Artefact\nInterface" as thingartefactinterface #grey
	control "Remove Artefacts\nSuch As\nBright Stars" as controlremoveartefact
	control "Remove Artefacts\nSuch As\nCamera" as controlremoveartefactcamera
	control "Perform Prefiltering\nOf The\nImages" as controlperformprefilteringremoveartefactremoveartefactcamera

	thingbrightstar <.. thingartefact
	thingcamera <.. thingartefact
	thingimage *-- thingprefiltering
	actorzooniverseadmin --- thingimageinterface
	actorzooniverseadmin --- thingartefactinterface
	thingartefact --- controlremoveartefact
	thingbrightstar --- controlremoveartefact
	thingimageinterface --> controlremoveartefact
	controlperformprefilteringremoveartefactremoveartefactcamera --> controlremoveartefact
	thingcamera --- controlremoveartefactcamera
	controlperformprefilteringremoveartefactremoveartefactcamera --> controlremoveartefactcamera
	thingartefactinterface --> controlremoveartefactcamera
	thingartefact --- controlremoveartefactcamera
	thingartefact --- controlperformprefilteringremoveartefactremoveartefactcamera
	thingprefiltering --- controlperformprefilteringremoveartefactremoveartefactcamera
	thingimage --- controlperformprefilteringremoveartefactremoveartefactcamera
	thingimageinterface --> controlperformprefilteringremoveartefactremoveartefactcamera

@enduml
from typing import Dict, List, Type, Iterable, Any
from importlib.util import find_spec
from spacy.tokens.doc import Doc
from spacy.tokens.token import Token


use_src = find_spec("src")

if use_src:
    from src.storyparser.nlp_enums import *
else:
    from storyparser.nlp_enums import *


class Node:

    def __init__(self, token: Token, parent: "Node"=None, mods: List["Node"]=None):
        #self.ne: str = node_dict["NE"]
        self.pos_coarse: str = token.pos_
        self._pos_fine: Type["POSFine"] = POSFine.from_str(token.tag_)
        self._arc: Type["Arc"] = Arc.from_str(token.dep_)
        self.lemma: str = token.lemma_
        self.word: str = token.text
        self._modifiers: List["Node"] = []
        if mods:
            self.add_modifiers(mods)
        self.parent: "Node" = parent
        self.ignore = False
        self.token = token
        self.doc = token.doc
        self.index = token.i

    def get_global_sent(self)-> str:
        return str(self.doc.text)

    @property
    def arc(self) -> Type["Arc"]:
        return self._arc

    @arc.setter
    def arc(self, val: Any):
        if type(val) == str:
            self._arc = Arc(val)
        elif type(val) == Arc:
            self._arc = val
        else:
            raise TypeError("Not a valid arc value")

    @property
    def pos_fine(self) -> Type["POSFine"]:
        return self._pos_fine

    @pos_fine.setter
    def pos_fine(self, val: Any):
        if type(val) == str:
            self._pos_fine = POSFine(val)
        elif type(val) == POSFine:
            self._pos_fine = val
        else:
            raise TypeError("Not a valid arc value")

    @property
    def modifiers(self) -> List["Node"]:
        return list(self._modifiers)

    def remove_modifier(self, mod: "Node") -> bool:
        if mod in self._modifiers:
            mod.parent = None
            self._modifiers.remove(mod)
            return True
        return False

    def add_modifier(self, mod: "Node"):
        mod.parent.remove_modifier(mod)
        self._modifiers.append(mod)
        mod.parent = self

    def add_modifiers(self, mods: List["Node"]):
        for mod in mods:
            self.add_modifier(mod)

    def copy(self, include_children: bool=True) -> "Node":
        node_dict = {
            "NE": self.ne,
            "POS_coarse": self.pos_coarse,
            "POS_fine": self.pos_fine.value,
            "arc": self.arc.value,
            "lemma": self.lemma,
            "word": self.word
        }
        if include_children:
            new_node = Node(node_dict, self.parent, [mod.copy() for mod in self.modifiers])
        else:
            new_node = Node(node_dict, self.parent, [])
        return new_node

    def __str__(self):
        if self.parent:
            parent = self.parent.word
        else:
            parent = "NO PARENT"
        return "{" + \
               "\n    POS_coarse: " + self.pos_coarse + \
               "\n    POS_fine: " + str(self.pos_fine) + \
               "\n    arc: " + str(self._arc) + \
               "\n    lemma: " + self.lemma + \
               "\n    word: " + self.word + \
               "\n    modifiers: " + str([node.word for node in self.modifiers]) + \
               "\n    parent: " + parent + \
               "\n}"

    def any_modifiers_pos_fine(self,POS_fine_str):
        """
        Return True,  si dans les subtree des modifiers il y a au moins 1 node avec un pos_fine == POS_fine
        sinon False
        """
        POS_fine = POSFine.from_str(POS_fine_str)
        for m in self.modifiers:
            if(m._pos_fine == POS_fine):
                print("On l a :",m.word)
                return True
            if(m.any_modifiers_pos_fine(POS_fine_str)):
                return True
         
        return False


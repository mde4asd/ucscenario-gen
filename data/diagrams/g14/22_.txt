
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "World" as thingworld
	entity "Publish Datum" as thingpublisheddatum
	actor "Publisher" as actorpublisher
	boundary "World\nInterface" as thingworldinterface #grey
	control "Show The\nWorld" as controlshowworld

	actorpublisher --- thingworldinterface
	thingworld --- controlshowworld
	thingworldinterface --> controlshowworld

@enduml
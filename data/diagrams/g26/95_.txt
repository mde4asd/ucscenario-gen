
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Rac Staff" as thingracstaff #grey
	entity "User Groups" as thingusergroups
	actor "Administrator" as actoradministrator
	boundary "Groups\nInterface" as thinggroupsinterface #grey
	control "Assign Rac\nStaff To\nDifferent User\nGroups" as controlassignracstaffdifferentusergroups

	thingusergroups <.. thingracstaff
	actoradministrator --- thinggroupsinterface
	thingracstaff --- controlassignracstaffdifferentusergroups
	thingusergroups --- controlassignracstaffdifferentusergroups
	thinggroupsinterface --> controlassignracstaffdifferentusergroups

@enduml

@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Course" as thingothercourse
	actor "Site Visitor" as actorsitevisitor
	boundary "Course\nInterface" as thingcourseinterface #grey
	control "See A\nList Of" as controlseelist
	control "Page Through\nA List\nOf All\nUpcoming Other\nCourses" as controlpagelist #grey
	control "Upcome Other\nCourses" as controlupcomeothercourse

	actorsitevisitor --- thingcourseinterface
	thingcourseinterface --> controlseelist
	thingothercourse --- controlpagelist
	controlseelist --> controlupcomeothercourse
	thingothercourse --- controlupcomeothercourse
	thingcourseinterface --> controlupcomeothercourse

@enduml
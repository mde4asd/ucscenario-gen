
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Assign Order" as thingassignedorder
	entity "Object" as thingobject
	actor "Dams Manager" as actordamsmanager
	boundary "Component" as thingcomponentcompriseobjectdamsmanager #grey
	control "Present In\nAssigned Order" as controlpresentassignedordercomponentcompriseobjectdamsmanager
	control "Comprise An\nObject" as controlcompriseobject

	actordamsmanager --- thingcomponentcompriseobjectdamsmanager
	thingassignedorder --- controlpresentassignedordercomponentcompriseobjectdamsmanager
	thingcomponentcompriseobjectdamsmanager --> controlpresentassignedordercomponentcompriseobjectdamsmanager
	controlpresentassignedordercomponentcompriseobjectdamsmanager --> controlcompriseobject
	thingobject --- controlcompriseobject
	thingcomponentcompriseobjectdamsmanager --> controlcompriseobject

@enduml
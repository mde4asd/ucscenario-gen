
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Activity Fees" as thingactivityfees
	actor "Company Accountant" as actorcompanyaccountant
	boundary "Fees\nInterface" as thingfeesinterface #grey
	control "View All\nAvailable Activity\nFees" as controlviewavailableactivityfees

	actorcompanyaccountant --- thingfeesinterface
	thingactivityfees --- controlviewavailableactivityfees
	thingfeesinterface --> controlviewavailableactivityfees

@enduml
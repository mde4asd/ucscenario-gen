
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Manager" as thingmanager
	entity "Supply" as thingsupply
	entity "Camp" as thingcamp
	actor "Camp Worker" as actorcampworker
	boundary "Camp\nInterface" as thingcampinterface #grey
	control "Report A\nList Of\nSupplies To\nThe Manager\nThe Camp\nShort" as controlreportlistcamp

	thingcamp <.. thingmanager
	thingmanager <.. thingsupply
	actorcampworker --- thingcampinterface
	thingsupply --- controlreportlistcamp
	thingmanager --- controlreportlistcamp
	thingcamp --- controlreportlistcamp
	thingcampinterface --> controlreportlistcamp

@enduml
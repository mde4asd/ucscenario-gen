
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "News Items" as thingnewsitems
	entity "Mailing List" as thingmailinglist
	actor "User" as actoruser
	boundary "System" as thingsystemuser
	control "Forward Important\nNews Items\nTo An\nExternal Mailing\nList" as controlforwardsystemuserimportantnewsitems
	control "Have" as controlhaveforwardsystemuserimportantnewsitems

	actoruser --- thingsystemuser
	thingsystemuser --> controlforwardsystemuserimportantnewsitems
	thingnewsitems --- controlforwardsystemuserimportantnewsitems
	thingmailinglist --- controlforwardsystemuserimportantnewsitems
	controlhaveforwardsystemuserimportantnewsitems --> controlforwardsystemuserimportantnewsitems
	thingsystemuser --> controlhaveforwardsystemuserimportantnewsitems

@enduml
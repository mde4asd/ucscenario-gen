
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "File" as thingfile
	entity "Web Interface" as thingwebinterface #grey
	actor "Researcher" as actorresearcher
	boundary "Log Book\nPage" as thinglogbookpageusewebinterfaceresearcher #grey
	control "Upload Files\nHaving Files\nTo" as controluploadfilefile
	control "Use The\nWeb Interface" as controlusewebinterface

	actorresearcher --- thinglogbookpageusewebinterfaceresearcher
	thinglogbookpageusewebinterfaceresearcher --> controluploadfilefile
	thingfile --- controluploadfilefile
	controluploadfilefile --> controlusewebinterface
	thingwebinterface --- controlusewebinterface
	thinglogbookpageusewebinterfaceresearcher --> controlusewebinterface

@enduml
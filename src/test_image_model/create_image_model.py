import sys
sys.path.insert(0, "../")
from src.storyparser.tree_parser import TreeParser
from src.uml.diagram import Diagram
from src.util import Logger
from src.storyparser.entity import Entity, EntityType, Property
import os

from src.preprocessing.preprocessing import Preprocessing

import spacy
import subprocess
from importlib.util import find_spec
from datetime import datetime
from typing import List
import time
import traceback

import pandas as pd

plant_uml = os.path.abspath("../plant_uml/plantuml.jar")
nlp = spacy.load("en")
path_save1 = "./djangoserver/out/test1/"
path_save2 = "./djangoserver/out/test2/"

def parse(story: str, logger: Logger, parser: TreeParser=None):
    if not parser:
        parser = TreeParser()
    doc_spacy = nlp(story)
    print("DOC: ",doc_spacy)
    #dependancy_tree = doc_spacy.print_tree()
    parser.entities(doc_spacy, logger,nlp)
    print("parser", parser)


    return parser
def print_log(logger, story_id):
    logger.print_to_file(
        os.path.abspath(os.path.join(os.path.dirname(__file__), "..", "out")),
        story_id + "_LOG.txt"
    )


def make(parser, logger, id, stories: List[str]=None, display_all: bool=False, type_dia="single", path_save=".",df_out=None):
    try:
        diagram = Diagram(parser.all_ents, parser.ents_by_story, logger, notes=stories, display_all=display_all)

        diagram_out = os.path.abspath(path_save+str(id)+"_"+".txt")
    
        #os.path.abspath(os.path.join(os.path.dirname(__file__), "..", "out", type_dia, id + ".txt"))
        diagram_file = open(diagram_out, "w+")

        diagram_file.write(diagram.textual_representation)
        diagram_file.close()
        time.sleep(0.2)

        print(plant_uml)
        print(diagram_out)
        print(subprocess.check_output(["bash", "-c", "java -jar '" + plant_uml + "' '" + diagram_out + "'"]))
        print(" on a créé le fichier : ",diagram_out)
        print("INFO NB_ ", diagram.count_do())
            



        return diagram.count_do()
    except Exception as e:
        print(" on n'a pas créé le fichier : ",e)

        logger.log("error", traceback.format_exc())
        return None

"""
    print("ents : ")
    for e in parser.all_ents:
        e.print()
        if(e.entity_type == EntityType.composite):
            print("COMPOSIT")
            e.get_children_relation()
        print("-----")
    print([n.root for n in parser.all_ents])

    diagram = Diagram(parser.all_ents, parser.ents_by_story, logger, notes=stories, display_all=display_all)
    
    if(name_file_DO):
        ##Create CSV with all Diagrame Object
        bases = [r.base for r in diagram.bases.values()]
        base_variants = [v for r in diagram.bases.values() for v in r.variations.values()]
        list_base = [do for do in bases + base_variants + diagram.loose_diagram_objects]
        create_df_diagram_object(list_base,name_file_DO)
        print("ON a add : ",name_file_DO)
        #####################
    i=0
    for do in diagram._all_list_do:
        textual = diagram.sub_set_by_view(do)
        if textual:
            try:
                i+=1
                if(name_file_DO not in os.listdir(path_save+"view_actor/")):
                    os.mkdir(path_save+"view_actor/"+name_file_DO+"/")
                path_save_file=path_save+"view_actor/"+name_file_DO+"/"
                diagram_out = os.path.abspath(path_save_file+"view_"+str(i)+"_"+do.pretty_name+".txt")
                diagram_file = open(diagram_out, "w+")
                diagram_file.write(textual)
                diagram_file.close()
                time.sleep(0.2)
                print("Finish "+str(i))
                print(subprocess.check_output(["bash", "-c", "java -jar '" + plant_uml + "' '" + diagram_out + "'"]))
            except Exception as e:
                #print("on a un problème au MAKE")
                #print("error", traceback.format_exc())
                logger.log("error", traceback.format_exc())
            
    d.dd()
    for i in range (0,len(diagram.sub_textual_representation)):
        try:
            if(name_file_DO not in os.listdir(path_save)):
                os.mkdir(path_save+name_file_DO+"/")
            path_save_file=path_save+name_file_DO+"/"
            diagram_out = os.path.abspath(path_save_file+str(i)+".txt")
            diagram_file = open(diagram_out, "w+")
            diagram_file.write(diagram.sub_textual_representation[i])
            diagram_file.close()
            time.sleep(0.2)
            print("Finish "+str(i))
            print(subprocess.check_output(["bash", "-c", "java -jar '" + plant_uml + "' '" + diagram_out + "'"]))
        except Exception as e:
            print("on a un problème au MAKE")
            print("error", traceback.format_exc())
            logger.log("error", traceback.format_exc())
"""
def generate(story: str,story_id,path_save ="./"):
    try:
        #story_id = story.replace(" ","_")
        print("SOTRY_ID ", story_id)
        print("STORY : ",story)
        parser = TreeParser()

        logger = Logger(story_id)
        story = story.replace("\n", "")
        story_preprocess = Preprocessing().process([story])
        story_split,_ = story_preprocess[0]
        print("story_split ",story_split)
        for s in story_split:
            print("begin :",s)
            p = parse(s, logger, parser=parser)
            print("end :",p)
        count=make(p, logger, story_id, stories=[story],path_save=path_save,display_all=True)
        return   count

    except AssertionError:
        add_df_analyse("assertionError",story,"","")
        return  None
    except AttributeError:
        add_df_analyse("assertionError",story,"","")
        return  None

    except TypeError:
        add_df_analyse("assertionError",story,"","")
        return  None

    except IndexError:
        add_df_analyse("assertionError",story,"","")
        return  None
def multi(stories: List[str], display_all: bool=True,path_save ="./",name_file_DO=None):
        print("DIAGRAM_TYPE", display_all)
        print("MULTI_US")
        parser = TreeParser()
        story_id = str(datetime.now().timestamp())
        logger = Logger(story_id)
        stories_split = []
        for story in stories:
            print(story)
            if(story==story):
                story = story.replace("\n", "")
                
                for  s in story.split("."):
                    if(len(s) > 1):
                        s = s + "."
                        stories_split.append(s)
        stories = stories_split
        stories = Preprocessing().clean_symbol(stories)

        list_note = []
        for story in stories:
            story_preprocess = Preprocessing().process([story])
            story_split,_ = story_preprocess[0]
            try:
                for s in story_split:
                    p = parse(s, logger, parser=parser)
                list_note+= [story for i in range(0,len(story_split))]
            except IndexError:
                print("IndexError :",s)
        make(p, logger, story_id, stories=list_note,path_save=path_save, display_all=True,name_file_DO=name_file_DO)

        #DiagramGenerator.make(parser, logger, story_id, stories=stories, display_all=display_all)
        #print_log(logger, story_id)
        return story_id

###TODO REMOVE Only for test ###
import pandas as pd
import os
def add_df_analyse(name,us,word,output):
    print("PATH : ",os.listdir())
    if("djangoserver"in os.listdir()):
        path = "./djangoserver/out/test_rule/"+name+".csv"
    else:
        path = "./out/test_rule/"+name+".csv"
    try:
        df = pd.read_csv(path)
    except FileNotFoundError:
        df = pd.DataFrame(columns = ['us' , 'word', 'output' ])
    df = df.append({'us' : us , 'word' : word,'output':output} , ignore_index=True)

    df.to_csv(path,index=False)

###TODO REMOVE Only for test ###
def create_df_diagram_object(list_object,name):
    
    print("PATH Diagram : ",os.listdir())
    
    #name = "diagram_object_list"
    if("djangoserver" in os.listdir()):
        path = "./djangoserver/out/test_diagram_object/"+name+"_diag_obj.csv"
    else:
        path = "./out/test_diagram_object/"+name+".csv"

    df = pd.DataFrame(columns = ['description' , 'word', 'type' ])

    for obj in list_object:
        print(obj)
        df = df.append({'description' : str(obj.pretty_name) , 'word' : str(obj.root),'type':str(obj.object_type)} , ignore_index=True)

    df.to_csv(path,index=False)
    print("on ajoute le csv :",path)


path_us = "./test_image_model/us_to_display.csv"
path_save="./djangoserver/out/"
path_save = path_save + sys.argv[1]
multi_us =  sys.argv[2]
name = sys.argv[3]
print("multi_us ",multi_us)

all_us = pd.read_csv(path_us)["us"]
if(multi_us):
    if(name):
        all_us = pd.read_csv("../data/split_by_project/"+name+".csv")["User story"]
        list_count = []
        for i in range(0,len(all_us)):
            count = generate(all_us[i],path_save=path_save,story_id=i)

            if(count):
                list_count.append([all_us[i],count[0], count[1], count[2]])
            else:
                list_count.append([all_us[i],"nb_do_tot", "nb_do_connect", "nb_do_unconnect"])
        df_out = pd.DataFrame()
        for val in list_count:
            print("us ", val)
            print("ddd",val[1])
        df_out["us"] = [val[0] for val in list_count]       
        df_out["Total Diagram Object"] = [val[1] for val in list_count]
        df_out["Connect Diagram Object"] = [val[2] for val in list_count]
        df_out["UnConnect Diagram Object"] = [val[3] for val in list_count]
        

        print("on save ici : ",path_save)
        df_out.to_csv(path_save+name+"_count.csv",index=False)
    #multi(all_us,path_save=path_save,name_file_DO=name)
else:
    for i in range(0,len(all_us)):
        print(all_us[i])
        generate(all_us[i],path_save=path_save,story_id=i)
        

print(path_save)
print(os.listdir("./"))



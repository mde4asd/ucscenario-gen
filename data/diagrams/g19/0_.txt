
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Alfred" as thingALFRED
	entity "Personal Datum" as thingpersonaldatum
	entity "Specific Permission" as thingspecificpermission
	actor "Olderperson" as actorOlderPerson
	boundary "Permission\nInterface" as thingpermissioninterface #grey
	control "Know" as controlknow
	control "Share Personal\nData Only\nOn Specific\nPermission" as controlsharepersonaldatumspecificpermission

	thingspecificpermission <.. thingpersonaldatum
	actorOlderPerson --- thingpermissioninterface
	thingspecificpermission --- controlsharepersonaldatumspecificpermission
	thingpermissioninterface --> controlsharepersonaldatumspecificpermission
	thingpersonaldatum --- controlsharepersonaldatumspecificpermission

@enduml
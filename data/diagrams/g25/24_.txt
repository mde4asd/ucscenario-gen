
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Object" as thingobject
	entity "Rights Pertinent" as thingrightspertinent
	actor "Repository Manager" as actorrepositorymanager
	boundary "Object\nInterface" as thingobjectinterface #grey
	control "Indicate Any\nOther Rights\nPertinent To\nAn Object" as controlindicateotherrightspertinent

	thingobject <.. thingrightspertinent
	actorrepositorymanager --- thingobjectinterface
	thingrightspertinent --- controlindicateotherrightspertinent
	thingobject --- controlindicateotherrightspertinent
	thingobjectinterface --> controlindicateotherrightspertinent

@enduml

@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Center Information" as thingcenterinformation #grey
	actor "Admin" as actoradmin
	boundary "Information\nInterface" as thinginformationinterface #grey
	control "Recycling Center\nInformation" as controlrecyclecenterinformation
	control "Add" as controladdrecyclecenterinformation

	actoradmin --- thinginformationinterface
	thingcenterinformation --- controlrecyclecenterinformation
	controladdrecyclecenterinformation --> controlrecyclecenterinformation
	thinginformationinterface --> controlrecyclecenterinformation
	thinginformationinterface --> controladdrecyclecenterinformation

@enduml
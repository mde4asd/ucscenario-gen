
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Dams Managers" as thingdamsmanagers #grey
	entity "Universal Access" as thinguniversalaccess
	entity "Curatorial Access" as thingcuratorialaccess
	entity "Units Assets" as thingunitsassets #grey
	entity "Staff Members" as thingstaffmembers #grey
	entity "Unit" as thingunit
	actor "Repository Manager" as actorrepositorymanager
	boundary "Access\nInterface" as thingaccessinterface #grey
	control "Constrain Curatorial\nAccess To\nUnits Assets\nTo Staff\nMembers Of\nUnit Dams\nManagers To\nHaving Universal\nAccess" as controlconstraincuratorialaccessstaffmembersdamsmanagersuniversalaccess

	thinguniversalaccess <.. thingdamsmanagers
	thingunitsassets <.. thingcuratorialaccess
	thingstaffmembers <.. thingunitsassets
	thingunit <.. thingstaffmembers
	thingdamsmanagers <.. thingunit
	actorrepositorymanager --- thingaccessinterface
	thingcuratorialaccess --- controlconstraincuratorialaccessstaffmembersdamsmanagersuniversalaccess
	thingunitsassets --- controlconstraincuratorialaccessstaffmembersdamsmanagersuniversalaccess
	thingstaffmembers --- controlconstraincuratorialaccessstaffmembersdamsmanagersuniversalaccess
	thingunit --- controlconstraincuratorialaccessstaffmembersdamsmanagersuniversalaccess
	thingdamsmanagers --- controlconstraincuratorialaccessstaffmembersdamsmanagersuniversalaccess
	thinguniversalaccess --- controlconstraincuratorialaccessstaffmembersdamsmanagersuniversalaccess
	thingaccessinterface --> controlconstraincuratorialaccessstaffmembersdamsmanagersuniversalaccess

@enduml
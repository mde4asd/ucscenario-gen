
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Object" as thingobject
	entity "Item" as thingitem
	entity "Repository" as thingrepository
	entity "Archivesspace" as thingArchivesSpace
	actor "Collection Curator" as actorcollectioncurator
	circle "Status" as thingstatus
	boundary "Archivesspace\nInterface" as thingarchivesspaceinterface #grey
	control "Deposit Items\nInto The\nRepository Within\nArchivesspace" as controldeposititemrepositoryArchivesSpace
	control "View The\nStatus Of\nThose Objects\nIn Archivesspace" as controlviewstatusArchivesSpace #grey

	thingrepository <.. thingitem
	thingArchivesSpace <.. thingrepository
	thingobject *-- thingstatus
	actorcollectioncurator --- thingarchivesspaceinterface
	thingitem --- controldeposititemrepositoryArchivesSpace
	thingrepository --- controldeposititemrepositoryArchivesSpace
	thingarchivesspaceinterface --> controldeposititemrepositoryArchivesSpace
	thingArchivesSpace --- controldeposititemrepositoryArchivesSpace
	thingstatus --- controlviewstatusArchivesSpace
	thingobject --- controlviewstatusArchivesSpace
	thingArchivesSpace --- controlviewstatusArchivesSpace

@enduml
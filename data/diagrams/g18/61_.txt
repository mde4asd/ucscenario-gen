
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Group" as thinggroup
	entity "Individual" as thingindividual
	entity "Logbook Entry" as thinglogbookentry
	actor "User" as actoruser
	boundary "Entry\nInterface" as thingentryinterface #grey
	boundary "Group\nInterface" as thinggroupinterface #grey
	control "Keep A\nPrivate Logbook\nEntry" as controlkeepprivatelogbookentry
	control "Share A\nPrivate Logbook\nEntry With\nIndividuals Rather\nThan Groups" as controlshareprivatelogbookentry

	thinggroup <.. thingindividual
	thingindividual <.. thinglogbookentry
	actoruser --- thingentryinterface
	actoruser --- thinggroupinterface
	thinglogbookentry --- controlkeepprivatelogbookentry
	thingentryinterface --> controlkeepprivatelogbookentry
	thingindividual --- controlshareprivatelogbookentry
	thinggroup --- controlshareprivatelogbookentry
	thinggroupinterface --> controlshareprivatelogbookentry
	thinglogbookentry --- controlshareprivatelogbookentry

@enduml
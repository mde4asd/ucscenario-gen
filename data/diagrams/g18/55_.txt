
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Neurohub" as thingNeuroHub
	entity "Current Version" as thingcurrentversion
	entity "Datum" as thingdatum
	actor "System Administrator" as actorsystemadministrator
	circle "Old Version" as thingolderversion
	boundary "Version\nInterface" as thingversioninterface #grey
	control "Migrate Data\nFrom An\nOlder Version\nOf Neurohub\nTo The\nCurrent Version" as controlmigratedatumolderversioncurrentversion

	thingcurrentversion <.. thingNeuroHub
	thingolderversion <.. thingdatum
	thingNeuroHub *-- thingolderversion
	actorsystemadministrator --- thingversioninterface
	thingdatum --- controlmigratedatumolderversioncurrentversion
	thingolderversion --- controlmigratedatumolderversioncurrentversion
	thingNeuroHub --- controlmigratedatumolderversioncurrentversion
	thingcurrentversion --- controlmigratedatumolderversioncurrentversion
	thingversioninterface --> controlmigratedatumolderversioncurrentversion

@enduml

@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "App" as thingapp
	entity "Version Conflict\nError" as thingversionconflicterror #grey
	entity "Type" as thingtype
	entity "App" as thingotherapp
	entity "Dataset Instance" as thingdatasetinstance #grey
	entity "Dataset" as thingdataset #grey
	actor "App Developer" as actorappdeveloper
	circle "Different Version" as thingdifferentversion
	circle "New Version" as thingnewversion
	boundary "Deployment" as thingdeployment
	boundary "Version\nInterface" as thingversioninterface #grey
	boundary "Type\nInterface" as thingtypeinterface #grey
	control "Ensure" as controlensure
	control "Deploy A\nNew Version\nOf An\nApp Includes\nA Different\nVersion Of\nA Dataset\nType Another\nApp Shares\nA Dataset\nInstance Of\nThis Type\nWith The\nOther App\nThe Deployment\nFail With\nA Version\nConflict Error" as controldeploydatasetinstanceotherappensuredeploymentversionconflicterrornewversion
	control "Deploy A\nNew Version\nOf An\nApp Includes\nA Different\nVersion Of\nA Type\nThis App\nShares" as controldeployensuremoinsPRONmoinsnewversionapp

	thingdifferentversion <.. thingapp
	thingnewversion <.. thingversionconflicterror
	thingotherapp <.. thingtype
	thingdataset <.. thingtype
	thingapp <.. thingtype
	thingtype <.. thingdatasetinstance
	thingtype *-- thingdifferentversion
	thingapp *-- thingnewversion
	actorappdeveloper --- thingdeployment
	actorappdeveloper --- thingversioninterface
	controldeploydatasetinstanceotherappensuredeploymentversionconflicterrornewversion --> controlensure
	thingversioninterface --> controlensure
	controldeployensuremoinsPRONmoinsnewversionapp --> controlensure
	thingdatasetinstance --- controldeploydatasetinstanceotherappensuredeploymentversionconflicterrornewversion
	thingtype --- controldeploydatasetinstanceotherappensuredeploymentversionconflicterrornewversion
	thingotherapp --- controldeploydatasetinstanceotherappensuredeploymentversionconflicterrornewversion
	thingdeployment --> controldeploydatasetinstanceotherappensuredeploymentversionconflicterrornewversion
	thingversionconflicterror --- controldeploydatasetinstanceotherappensuredeploymentversionconflicterrornewversion
	thingnewversion --- controldeploydatasetinstanceotherappensuredeploymentversionconflicterrornewversion
	thingdifferentversion --- controldeploydatasetinstanceotherappensuredeploymentversionconflicterrornewversion
	thingapp --- controldeploydatasetinstanceotherappensuredeploymentversionconflicterrornewversion
	thingtypeinterface --> controldeployensuremoinsPRONmoinsnewversionapp
	thingdifferentversion --- controldeployensuremoinsPRONmoinsnewversionapp
	thingnewversion --- controldeployensuremoinsPRONmoinsnewversionapp
	thingapp --- controldeployensuremoinsPRONmoinsnewversionapp
	thingtype --- controldeployensuremoinsPRONmoinsnewversionapp

@enduml
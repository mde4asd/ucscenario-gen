
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Access" as thingaccess
	entity "Content Files" as thingcontentfiles #grey
	entity "Object" as thingobject
	actor "User" as actoruser
	boundary "Object\nInterface" as thingobjectinterface #grey
	control "Have Access\nTo The\nContent Files\nFor An\nObject" as controlhaveaccess

	thingcontentfiles <.. thingaccess
	thingobject <.. thingaccess
	thingobject <.. thingcontentfiles
	actoruser --- thingobjectinterface
	thingaccess --- controlhaveaccess
	thingcontentfiles --- controlhaveaccess
	thingobject --- controlhaveaccess
	thingobjectinterface --> controlhaveaccess

@enduml
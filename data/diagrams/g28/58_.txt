
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Different Project" as thingdifferentproject
	entity "Volunteer" as thingvolunteer
	entity "Previous Experience" as thingpreviousexperience
	actor "Zooniverse Admin" as actorzooniverseadmin
	boundary "Experience\nInterface" as thingexperienceinterface #grey
	control "Recommend Different\nProjects To\nVolunteers On\nPrevious Experiences" as controlrecommenddifferentprojectvolunteerpreviousexperience

	thingvolunteer <.. thingdifferentproject
	thingpreviousexperience <.. thingvolunteer
	actorzooniverseadmin --- thingexperienceinterface
	thingdifferentproject --- controlrecommenddifferentprojectvolunteerpreviousexperience
	thingvolunteer --- controlrecommenddifferentprojectvolunteerpreviousexperience
	thingpreviousexperience --- controlrecommenddifferentprojectvolunteerpreviousexperience
	thingexperienceinterface --> controlrecommenddifferentprojectvolunteerpreviousexperience

@enduml
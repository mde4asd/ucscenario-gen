import os
import spacy
from importlib.util import find_spec

use_src = find_spec("src")

if use_src:
    from src.storyparser.file_reader import FileReader
    from src.storyparser.tree_parser import TreeParser
    from src.uml.diagram import Diagram
    from src.util import Logger
else:
    from storyparser.file_reader import FileReader
    from storyparser.tree_parser import TreeParser
    from uml.diagram import Diagram
    from util import Logger


def main():
    working_directory = os.path.dirname(__file__)
    raw = os.path.join(working_directory, "..", "userstories", "stories.yaml")
    filepath = os.path.abspath(raw)
    stories = FileReader.load(filepath)
    nlp = spacy.load("en")
    tp = TreeParser()
    for story in stories:
        logger = Logger(story.title)
        e, pa = tp.entities(nlp(story.asWantThat).print_tree(), logger)
        for name, d_type in [("default", False), ("full", True)]:
            filepath = os.path.abspath(
                os.path.join(
                    working_directory, "..", "out", story.title.replace(" ", "_") + "_" + name + ".txt"
                )
            )
            diagram = Diagram(e, [(e, pa)], logger, notes=[story.asWantThat], display_all=d_type)
            with open(filepath, "w+") as file:
                file.write(diagram.textual_representation)
            logger.print_to_file(
                os.path.join(working_directory, "..", "out", "logs"), story.title.replace(" ", "_") + "_" + name
            )

    for name, d_type in [("default", False), ("full", True)]:
        filepath = os.path.abspath(os.path.join(working_directory, "..", "out", "merged_" + name + ".txt"))
        logger = Logger("merged")
        with open(filepath, "w+") as file:
                file.write(Diagram(tp.all_ents, tp.ents_by_story, logger, display_all=d_type).textual_representation)


if __name__ == "__main__":
    main()

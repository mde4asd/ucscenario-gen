
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Record" as thingrecord
	entity "Associated File" as thingassociatedfile
	entity "Datum" as thingdatum
	entity "Collection" as thingcollection
	entity "System" as thingsystem
	entity "Uniform Error" as thinguniformerror
	actor "Support ,\nRepository ,\nTeam Member" as actorsupportrepositoryteammember
	boundary "File\nInterface" as thingfileinterface #grey
	boundary "Record\nInterface" as thingrecordinterface #grey
	boundary "Datum\nInterface" as thingdatuminterface #grey
	boundary "Collection\nInterface" as thingcollectioninterface #grey
	boundary "System\nInterface" as thingsysteminterface #grey
	control "Export A\nSet Of\nAssociated File" as controlexportsetassociatedfile
	control "Export A\nSet Of\nRecords" as controlexportset
	control "Manipulate This\nData" as controlmanipulatedatum
	control "Am Aware\nOf A\nUniform Error\nIn A\nCollection" as controlbeuniformerror
	control "Reload Into\nThe System" as controlreloadsystem

	thingcollection <.. thinguniformerror
	actorsupportrepositoryteammember --- thingfileinterface
	actorsupportrepositoryteammember --- thingrecordinterface
	actorsupportrepositoryteammember --- thingdatuminterface
	actorsupportrepositoryteammember --- thingcollectioninterface
	actorsupportrepositoryteammember --- thingsysteminterface
	thingassociatedfile --- controlexportsetassociatedfile
	thingfileinterface --> controlexportsetassociatedfile
	thingrecord --- controlexportset
	thingrecordinterface --> controlexportset
	thingdatum --- controlmanipulatedatum
	thingdatuminterface --> controlmanipulatedatum
	thinguniformerror --- controlbeuniformerror
	thingcollection --- controlbeuniformerror
	thingcollectioninterface --> controlbeuniformerror
	thingsystem --- controlreloadsystem
	thingsysteminterface --> controlreloadsystem

@enduml

@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Data Package" as thingdatapackage #grey
	entity "Old Version" as thingolderversion
	entity "Multiple Version" as thingmultipleversion
	actor "Publisher" as actorpublisher
	boundary "Package\nInterface" as thingpackageinterface #grey
	boundary "Version\nInterface" as thingversioninterface #grey
	control "Version Data\nPackage" as controlversiondatapackage
	control "Keep Multiple\nVersions Including\nOlder Versions" as controlkeepmultipleversionolderversion

	thingolderversion <.. thingmultipleversion
	actorpublisher --- thingpackageinterface
	actorpublisher --- thingversioninterface
	thingdatapackage --- controlversiondatapackage
	thingpackageinterface --> controlversiondatapackage
	thingmultipleversion --- controlkeepmultipleversionolderversion
	thingolderversion --- controlkeepmultipleversionolderversion
	thingversioninterface --> controlkeepmultipleversionolderversion

@enduml
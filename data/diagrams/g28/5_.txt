
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Available Tag" as thingavailabletag
	entity "Tag Video" as thingtaggedvideo
	entity "Processing Steps" as thingprocessingsteps
	actor "User" as actoruser
	boundary "Video\nInterface" as thingvideointerface #grey
	control "Have Available\nTags For\nAll Further\nProcessing Steps" as controlhaveavailabletag #grey
	control "Upload Tagged\nVideos" as controluploadtaggedvideo

	thingprocessingsteps <.. thingavailabletag
	actoruser --- thingvideointerface
	thingavailabletag --- controlhaveavailabletag
	thingprocessingsteps --- controlhaveavailabletag
	thingtaggedvideo --- controluploadtaggedvideo
	thingvideointerface --> controluploadtaggedvideo

@enduml
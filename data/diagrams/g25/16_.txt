
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Component" as thingcomponent
	entity "Object" as thingobject
	actor "Repository Manager" as actorrepositorymanager

	thingobject <.. thingcomponent

@enduml
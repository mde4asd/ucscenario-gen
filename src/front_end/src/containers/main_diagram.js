import React from 'react'
import {Container,Col,Row} from 'react-bootstrap';


const URL_IMG="http://localhost:5000/uploads/out/"

const Diagram = (props)=>  {
    const diagram=props.diagram;

    var plant_uml = "\n"
    if(diagram.plant_uml){
        plant_uml = diagram.plant_uml
    }
    console.log("diagram.title ",diagram.title)
    if(diagram.title == null){
        diagram.title=""
    }
        
    if(props.diagram.main_id){
        var URL_diagram = URL_IMG+diagram.type+"/"+diagram.main_id+"/"+diagram.id
    }else{
        var URL_diagram = URL_IMG+diagram.type+"/"+diagram.id
    }
    console.log(URL_diagram)   
    console.log("ID ",diagram.id)
    console.log("diagram ",diagram)
    
    if(diagram.id!=null){
        console.log("CREATE ",diagram.title)
        return (
            
            <Container>
                <Row>
                    <Col> <h5 className="sub_title" >{"Robustness diagrams "+diagram.title.toString().replace(/\\/, "/---/").replace("/---/n"," ")}</h5></Col>
                </Row>
                <Row>
                    <img  src ={`${URL_diagram}`}/>

                </Row>
                <Row>
                <div className="table_us">
                    <h5 className="sub_title" >User Stories</h5>
                    {diagram.us.map((item)=>(<div>{item}<br/> </div>))}
                </div>
                </Row>
                <Row >
                <div className="block_code">
                    <h5 className="sub_title" >Code PlantUML</h5>
                    {plant_uml.split("\n").map((item)=>(<div>{item}<br/> </div>))}
                </div>
                </Row>
            </Container>
            
    
            )
    }
else{
    return(<div></div>)
}
    
};


    



export default Diagram;

@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Report" as thingreport
	entity "Patron Use" as thingpatronuse #grey
	entity "Previous Version" as thingpreviousversion
	entity "Dataset" as thingdataset
	actor "Db Administrator" as actordbadministrator
	circle "New Version" as thingnewversion
	boundary "Use\nInterface" as thinguseinterface #grey
	boundary "Dataset\nInterface" as thingdatasetinterface #grey
	control "Upload New\nVersion Of\nReport Retaining\nPrevious Versions\nFor Patron\nUse" as controluploadnewversionpreviousversion
	control "Upload New\nVersion Of\nDataset" as controluploadnewversiondataset

	thingpreviousversion <.. thingreport
	thingpatronuse <.. thingpreviousversion
	thingreport *-- thingnewversion
	thingdataset *-- thingnewversion
	actordbadministrator --- thinguseinterface
	actordbadministrator --- thingdatasetinterface
	thingnewversion --- controluploadnewversionpreviousversion
	thingreport --- controluploadnewversionpreviousversion
	thinguseinterface --> controluploadnewversionpreviousversion
	thingpatronuse --- controluploadnewversionpreviousversion
	thingpreviousversion --- controluploadnewversionpreviousversion
	thingdataset --- controluploadnewversiondataset
	thingdatasetinterface --> controluploadnewversiondataset
	thingnewversion --- controluploadnewversiondataset

@enduml
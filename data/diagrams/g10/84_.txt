
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Payment" as thingpayment
	entity "Pende State" as thingpendingstate
	entity "People" as thingpeople
	entity "Trainer" as thingtrainer
	entity "Registry" as thingregistry
	entity "Class" as thingclass
	actor "Site Admin" as actorsiteadmin
	circle "Proof" as thingproof
	boundary "Trainer\nInterface" as thingtrainerinterface #grey
	control "Move People\nIn A\nTrainer's Class\nFrom A\nPending State\nTo The\nRegistry" as controlmovepeopleclasspendingstateregistry
	control "Received Proof\nOf Payment\nFrom The\nTrainer" as controlreceivemovepeopleclasspendingstateregistryprooftrainer

	thingtrainer <.. thingpayment
	thingregistry <.. thingpendingstate
	thingclass <.. thingpeople
	thingpendingstate <.. thingclass
	thingtrainer <.. thingclass
	thingpayment *-- thingproof
	actorsiteadmin --- thingtrainerinterface
	thingpeople --- controlmovepeopleclasspendingstateregistry
	thingclass --- controlmovepeopleclasspendingstateregistry
	thingpendingstate --- controlmovepeopleclasspendingstateregistry
	thingregistry --- controlmovepeopleclasspendingstateregistry
	controlreceivemovepeopleclasspendingstateregistryprooftrainer --> controlmovepeopleclasspendingstateregistry
	thingtrainerinterface --> controlmovepeopleclasspendingstateregistry
	thingproof --- controlreceivemovepeopleclasspendingstateregistryprooftrainer
	thingpayment --- controlreceivemovepeopleclasspendingstateregistryprooftrainer
	thingtrainer --- controlreceivemovepeopleclasspendingstateregistryprooftrainer
	thingtrainerinterface --> controlreceivemovepeopleclasspendingstateregistryprooftrainer

@enduml
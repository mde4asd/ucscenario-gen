
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Match ,\nService" as thingmatchmakingservice
	entity "Packaging Data" as thingpackagingdata #grey
	entity "App" as thingapp
	actor "Repositorymanagerresearcher" as actorrepositorymanagerresearcher
	boundary "Data\nInterface" as thingdatainterface #grey
	control "Provided With\nAn App\nActs As\nA Match,\nMaking Service\nFor Packaging\nData" as controlprovideapp

	thingpackagingdata <.. thingmatchmakingservice
	thingmatchmakingservice <.. thingapp
	actorrepositorymanagerresearcher --- thingdatainterface
	thingapp --- controlprovideapp
	thingmatchmakingservice --- controlprovideapp
	thingpackagingdata --- controlprovideapp
	thingdatainterface --> controlprovideapp

@enduml
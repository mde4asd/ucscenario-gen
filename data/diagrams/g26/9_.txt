
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Certain File" as thingcertainfile
	entity "Access" as thingaccess
	entity "User" as thinguser
	actor "Archivist" as actorarchivist
	boundary "User\nInterface" as thinguserinterface #grey
	control "Restrict Access\nTo Certain\nFiles By\nUser" as controlrestrictaccessuser

	thinguser <.. thingcertainfile
	thingcertainfile <.. thingaccess
	actorarchivist --- thinguserinterface
	thingaccess --- controlrestrictaccessuser
	thingcertainfile --- controlrestrictaccessuser
	thinguser --- controlrestrictaccessuser
	thinguserinterface --> controlrestrictaccessuser

@enduml
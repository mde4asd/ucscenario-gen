
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Record" as thingrecord
	entity "Repository" as thingrepository
	actor "Repository Manager" as actorrepositorymanager
	boundary "Repository\nInterface" as thingrepositoryinterface #grey
	control "Create Any\nRecords In\nRepository" as controlcreaterecord
	control "Update Any\nRecords In\nRepository" as controlupdaterecord
	control "Delete Any\nRecords In\nRepository" as controldeleterecordrepository

	thingrepository <.. thingrecord
	actorrepositorymanager --- thingrepositoryinterface
	thingrecord --- controlcreaterecord
	thingrepository --- controlcreaterecord
	thingrepositoryinterface --> controlcreaterecord
	thingrecord --- controlupdaterecord
	thingrepository --- controlupdaterecord
	thingrepositoryinterface --> controlupdaterecord
	thingrecord --- controldeleterecordrepository
	thingrepository --- controldeleterecordrepository
	thingrepositoryinterface --> controldeleterecordrepository

@enduml
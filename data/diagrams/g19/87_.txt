
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Alfred" as thingALFRED
	entity "User" as thinguser
	entity "Body Temperature" as thingbodytemperature #grey
	actor "Medicalcaregiver" as actorMedicalCaregiver
	boundary "Alfred\nInterface" as thingalfredinterface #grey
	boundary "Temperature\nInterface" as thingtemperatureinterface #grey
	control "Have To\nAlfred" as controlhaveALFRED
	control "Determine The\nUser's Body\nTemperature" as controldeterminebodytemperature

	actorMedicalCaregiver --- thingalfredinterface
	actorMedicalCaregiver --- thingtemperatureinterface
	thingALFRED --- controlhaveALFRED
	thingalfredinterface --> controlhaveALFRED
	thingbodytemperature --- controldeterminebodytemperature
	thingtemperatureinterface --> controldeterminebodytemperature

@enduml
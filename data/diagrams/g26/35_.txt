
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Image" as thingimage
	entity "Copyright Status" as thingcopyrightstatus #grey
	actor "Researcher" as actorresearcher
	boundary "Image\nInterface" as thingimageinterface #grey
	control "Understand The\nCopyright Status\nOf The\nImage" as controlunderstandcopyrightstatus

	thingimage <.. thingcopyrightstatus
	actorresearcher --- thingimageinterface
	thingcopyrightstatus --- controlunderstandcopyrightstatus
	thingimage --- controlunderstandcopyrightstatus
	thingimageinterface --> controlunderstandcopyrightstatus

@enduml
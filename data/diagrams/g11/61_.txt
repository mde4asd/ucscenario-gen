
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Recruiting Email\nTemplates" as thingrecruitingemailtemplates #grey
	actor "Team Member" as actorteammember
	boundary "Templates\nInterface" as thingtemplatesinterface #grey
	control "Have Recruiting\nEmail Templates" as controlhaverecruitingemailtemplatesgo
	control "Go" as controlgo

	actorteammember --- thingtemplatesinterface
	thingrecruitingemailtemplates --- controlhaverecruitingemailtemplatesgo
	thingtemplatesinterface --> controlhaverecruitingemailtemplatesgo
	controlhaverecruitingemailtemplatesgo --> controlgo
	thingtemplatesinterface --> controlgo

@enduml
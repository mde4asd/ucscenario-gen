
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Error Message" as thingerrormessage #grey
	entity "Accurate Text" as thingaccuratetext
	actor "Broker User" as actorbrokeruser
	boundary "Message\nInterface" as thingmessageinterface #grey
	control "Have Accurate\nText" as controlhaveaccuratetext
	control "Upload The\nError Message" as controluploaderrormessagehaveaccuratetext
	control "Validate The\nError Message" as controlvalidateerrormessagehaveaccuratetext

	actorbrokeruser --- thingmessageinterface
	thingaccuratetext --- controlhaveaccuratetext
	controluploaderrormessagehaveaccuratetext --> controlhaveaccuratetext
	thingmessageinterface --> controlhaveaccuratetext
	controlvalidateerrormessagehaveaccuratetext --> controlhaveaccuratetext
	thingerrormessage --- controluploaderrormessagehaveaccuratetext
	thingmessageinterface --> controluploaderrormessagehaveaccuratetext
	thingerrormessage --- controlvalidateerrormessagehaveaccuratetext
	thingmessageinterface --> controlvalidateerrormessagehaveaccuratetext

@enduml
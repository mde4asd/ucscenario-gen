
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "News Video\nSegments" as thingnewsvideosegments #grey
	entity "Segment" as thingsegment
	entity "Topic" as thingtopic
	actor "Admin" as actoradmin
	boundary "Segments\nInterface" as thingsegmentsinterface #grey
	boundary "Segment\nInterface" as thingsegmentinterface #grey
	control "Annotate News\nVideo Segments" as controlannotatenewsvideosegments
	control "Extract A\nTopic For\nEach Segment" as controlextracttopic

	thingsegment <.. thingtopic
	actoradmin --- thingsegmentsinterface
	actoradmin --- thingsegmentinterface
	thingnewsvideosegments --- controlannotatenewsvideosegments
	thingsegmentsinterface --> controlannotatenewsvideosegments
	thingtopic --- controlextracttopic
	thingsegment --- controlextracttopic
	thingsegmentinterface --> controlextracttopic

@enduml

@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Request" as thingrequest
	entity "Information" as thinginformation
	actor "Zoning ,\nStaff Member" as actorzoningstaffmember
	boundary "Information\nInterface" as thinginformationinterface #grey
	control "Process The\nRequests Zoning\nInformation" as controlprocessrequest

	thinginformation <.. thingrequest
	actorzoningstaffmember --- thinginformationinterface
	thingrequest --- controlprocessrequest
	thinginformation --- controlprocessrequest
	thinginformationinterface --> controlprocessrequest

@enduml
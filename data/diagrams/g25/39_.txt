
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Family" as thingfamily
	entity "Corporate Body" as thingcorporatebody
	entity "Component" as thingcomponent
	entity "Person" as thingperson
	actor "Repository Manager" as actorrepositorymanager
	boundary "Person\nInterface" as thingpersoninterface #grey
	boundary "Family\nInterface" as thingfamilyinterface #grey
	boundary "Body\nInterface" as thingbodyinterface #grey
	boundary "Component\nInterface" as thingcomponentinterface #grey
	control "Indicate Person" as controlindicateperson
	control "Indicate Family" as controlindicatefamily
	control "Indicate Corporate\nBody" as controlindicatecorporatebody
	control "Indicate Any\nName Is\nPertinent To\nThe Component" as controlindicatename

	actorrepositorymanager --- thingpersoninterface
	actorrepositorymanager --- thingfamilyinterface
	actorrepositorymanager --- thingbodyinterface
	actorrepositorymanager --- thingcomponentinterface
	thingperson --- controlindicateperson
	thingpersoninterface --> controlindicateperson
	thingfamily --- controlindicatefamily
	thingfamilyinterface --> controlindicatefamily
	thingcorporatebody --- controlindicatecorporatebody
	thingbodyinterface --> controlindicatecorporatebody
	thingcomponent --- controlindicatename
	thingcomponentinterface --> controlindicatename

@enduml
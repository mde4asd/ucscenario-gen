
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Life Cycle" as thinglifecycle #grey
	entity "Object" as thingobject
	actor "Repository Manager" as actorrepositorymanager
	boundary "Event" as thingeventrepositorymanager
	control "Know" as controlknowdoeventrepositorymanagerobject
	control "Done To\nAn Object\nOver The\nObject's Life\nCycle" as controldoeventrepositorymanagerobject

	thingobject <.. thinglifecycle
	thinglifecycle <.. thingobject
	actorrepositorymanager --- thingeventrepositorymanager
	thingeventrepositorymanager --> controlknowdoeventrepositorymanagerobject
	controlknowdoeventrepositorymanagerobject --> controldoeventrepositorymanagerobject
	thingeventrepositorymanager --> controldoeventrepositorymanagerobject
	thingobject --- controldoeventrepositorymanagerobject
	thinglifecycle --- controldoeventrepositorymanagerobject

@enduml
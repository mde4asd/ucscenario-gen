
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "App" as thingapp
	entity "Type" as thingtype
	entity "Dataset Instances" as thingdatasetinstances #grey
	entity "Dataset" as thingdataset #grey
	actor "Dataset Developer" as actordatasetdeveloper
	boundary "App" as thingappdatasetdeveloper
	boundary "App\nInterface" as thingappinterface #grey
	control "Deploy A\nIndependent Dataset\nType From\nAny App" as controldeployindependentdatasettype
	control "Allow" as controlallowcreateappdatasetdeveloperdatasetinstances
	control "Create Dataset\nInstances Of\nThat Type" as controlcreateappdatasetdeveloperdatasetinstances
	control "Use Dataset\nInstances Of\nThat Type" as controluseappdatasetdeveloperdatasetinstances

	thingdataset <.. thingtype
	thingapp <.. thingtype
	thingtype <.. thingdatasetinstances
	actordatasetdeveloper --- thingappdatasetdeveloper
	actordatasetdeveloper --- thingappinterface
	thingapp --- controldeployindependentdatasettype
	thingappinterface --> controldeployindependentdatasettype
	thingtype --- controldeployindependentdatasettype
	thingappdatasetdeveloper --> controlallowcreateappdatasetdeveloperdatasetinstances
	controlallowcreateappdatasetdeveloperdatasetinstances --> controlcreateappdatasetdeveloperdatasetinstances
	thingappdatasetdeveloper --> controlcreateappdatasetdeveloperdatasetinstances
	thingdatasetinstances --- controlcreateappdatasetdeveloperdatasetinstances
	thingtype --- controlcreateappdatasetdeveloperdatasetinstances
	thingtype --- controluseappdatasetdeveloperdatasetinstances
	thingappdatasetdeveloper --> controluseappdatasetdeveloperdatasetinstances
	thingdatasetinstances --- controluseappdatasetdeveloperdatasetinstances
	controlallowcreateappdatasetdeveloperdatasetinstances --> controluseappdatasetdeveloperdatasetinstances

@enduml
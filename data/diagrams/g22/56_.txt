
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Kind" as thingkind
	entity "Data Types" as thingdatatypes #grey
	entity "Type" as thingtype #grey
	actor "Data Manager" as actordatamanager
	control "Know" as controlknow

	thingdatatypes *-- thingkind
	thingtype *-- thingkind

@enduml
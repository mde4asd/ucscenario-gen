
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "File" as thingfile
	entity "Restriction End\nDate Information" as thingrestrictionenddateinformation #grey
	actor "Archivist" as actorarchivist
	boundary "File\nInterface" as thingfileinterface #grey
	control "Add Restriction\nEnd Date\nInformation To\nFiles" as controladdrestrictionenddateinformationfile

	thingfile <.. thingrestrictionenddateinformation
	actorarchivist --- thingfileinterface
	thingrestrictionenddateinformation --- controladdrestrictionenddateinformationfile
	thingfile --- controladdrestrictionenddateinformationfile
	thingfileinterface --> controladdrestrictionenddateinformationfile

@enduml
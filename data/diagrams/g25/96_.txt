
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Dam" as thingdams
	entity "Record" as thingrecord
	entity "Event" as thingevent
	actor "Dams Manager" as actordamsmanager
	boundary "Dams\nInterface" as thingdamsinterface #grey
	control "Track All\nEvents For\nRecording In\nThe Dams" as controltrackevent

	thingdams <.. thingrecord
	thingrecord <.. thingevent
	actordamsmanager --- thingdamsinterface
	thingevent --- controltrackevent
	thingrecord --- controltrackevent
	thingdams --- controltrackevent
	thingdamsinterface --> controltrackevent

@enduml
from typing import List, Dict, Set, Iterable, Optional, Union
from importlib.util import find_spec
from re import sub
import numpy as np
import nltk 
from nltk.corpus import wordnet 
import spacy 

#nlp = spacy.load('en_core_web_lg')
nlp = spacy.load('en_core_web_md')

use_src = find_spec("src")


if use_src:
    from src.uml.diagram_object import DiagramObject, ObjectType, BaseObject
    from src.storyparser.entity import *
    from src.util import pretty_print, Logger
else:
    from uml.diagram_object import DiagramObject, ObjectType, BaseObject
    from storyparser.entity import *
    from util import pretty_print, Logger


class ObjectStorage:

    def __init__(self):
        self._by_type = {
            ObjectType.actor: [],
            ObjectType.entity: [],
            ObjectType.boundary: [],
            ObjectType.control: [],
            ObjectType.usecase: [],
            ObjectType.circle: []
        }
        self.all = []
        self.num_to_pa = {}
        self.do_to_num = {}

    def __getitem__(self, item):
        if type(item) == ObjectType:
            return self._by_type[item]
        else:
            return sum(self._by_type[object_type] for object_type in item)

    def add(self, do: DiagramObject, story_number, primary_actor: bool=False):
        self._by_type[do.object_type].append(do)
        self.all.append(do)
        self.do_to_num[do] = story_number
        if primary_actor and do.object_type == ObjectType.actor:
            if story_number in self.num_to_pa:
                self.num_to_pa[story_number].append(do)
            else:
                self.num_to_pa[story_number] = [do]

    def primary_actors_for(self, do: DiagramObject):
        story_number = self.do_to_num[do]
        return self.num_to_pa[story_number]


def simple_tree(d, t):
    if t > 10:
        return ""
    res = "   " * t + "|..." + str(d)
    res += "\n"
    for x in d.ordered_parents:
        res += simple_tree(x, t + 1)
    return res


class Diagram:

    def __init__(self, raw: List[Entity], ents_by_story: List[Tuple[List[Entity], List[Entity]]], logger: Logger,
                 notes: List[str]=None, display_all: bool=False):
        self.bases: Dict[str, BaseObject] = {}
        self.loose_diagram_objects: List[DiagramObject] = []
        self.entity_to_object: Dict[Entity, DiagramObject] = {}
        self.raw: Iterable[Entity] = raw
        self.ents_by_story: List[Tuple[List[Entity], List[Entity]]] = ents_by_story
        self.objects = ObjectStorage()
        self._display_all = display_all # True => Extended Diagram with Property and Dependency link
        self.subset_diagram_object: List[set] = [] #List of sets containing the objects composing an independent diagram 
        self.notes = notes
        # generally speaking the process for creating diagrams is:
        #       Create all diagram objects from entities.
        #       Connect up diagram objects based on linguistic dependencies.
        #       Create any missing objects and re-arrange original connections as necessary.
        #       Merge all references to the same object.
        #       Remove any unreferenced non-explicit items.
        #       Build diagram.
        self._create_diagram_objects()
        
        self._connect_diagram_objects()
        
        self._resolve_diagram_objects()
       
        self._clean_up()

        out = ""
        for a in [x for x in self.objects.all if not x.ordered_children]:
            out += simple_tree(a, 0)
        
        bases = [r.base for r in self.bases.values()]
        base_variants = [v for r in self.bases.values() for v in r.variations.values()]
        self._all_list_do = bases + base_variants + self.loose_diagram_objects

        self._convert_is_entity_property_to_cirle()

        logger.log("Diagram Tree", out)

       
        self._merge_similarity_diagram_object()
       
        self.textual_representation, _ = self._create_textual_representation(self._all_list_do,notes=self.notes)
        self._create_subset_diagram_object()

        self.sub_textual_representation = [self._create_textual_representation(c,notes=self.notes) for c in [set_do for set_do in self.subset_diagram_object if self._is_valid_set(set_do)]]
        
        self.all_actor = [do for do in self._all_list_do if do.object_type == ObjectType.actor]
        self.all_boundary = [do for do in self._all_list_do if do.object_type == ObjectType.boundary]
        self.all_entity = [do for do in self._all_list_do if do.object_type == ObjectType.entity]
        self.all_control = [do for do in self._all_list_do if do.object_type == ObjectType.control]
        self.all_circle = [do for do in self._all_list_do if do.object_type == ObjectType.circle]
        
        logger.log("Diagram Text Version", self.textual_representation)


    @staticmethod
    def _note_representation(notes, width=6) -> str:
        note_str = ""
        for note_count, note in enumerate(notes):
            words = note.split(" ")
            chunks = []
            for i in range(0, len(words), width):
                chunks.append(" ".join(words[i:min(len(words), i+width)]))
            note_str += "\n\tnote \"" + "\\n".join(chunks) + "\" as loose_note_" + str(note_count) + "\n"
        return note_str

    def _resolve_diagram_objects(self):
        """
        For each specialization, we have a list of objects. 
        We will merge them to have only one single object linked to a specialisation
        """
        rejects = []
        for base_word, base in self.bases.items():

            # handle explicit objects first
            for do in [d for d in base.all_instances if d.explicit]:

                if not do.descriptors:
                    if base.base:
                        base.base.merge(do)
                    else:
                        base.base = do

                else:
                    if do.specialisation in base.variations:
                        base.variations[do.specialisation].merge(do)
                    else:
                        base.variations[do.specialisation] = do

            # merge in implicit objects if the explicit variant exists
            for do in [d for d in base.all_instances if not d.explicit]:

                # Assume that a specialisation is a valid condition for an object being referenced explicitly
                if do.root == do.specialisation:
                    if base.base:
                        base.base.merge(do)
                    elif self._display_all:
                        base.base = do

                else:
                    if do.specialisation in base.variations:
                        base.variations[do.specialisation].merge(do)
                    # basically for if there are multiple specialisations.
                    elif any(i.specialisation != do.specialisation for i in base.all_instances) or self._display_all:
                        base.variations[do.specialisation] = do

            if not base.base:
                # either there is one association => make it the base or there are multiple so create one.
                if not base.variations:
                    rejects.append(base_word)
                    for do in base.all_instances:
                        do.disconnect()
                    continue
                if len(base.variations) == 1:
                    _, base.base = base.variations.popitem()
                else:
                    base.base = DiagramObject()
                    base.base.pretty_name = base_word.title()
                    base.base.root = base_word
                    base.base._specialisation = base_word
                # not that the first case for previous if statements sets root.variations = []
                if len(base.variations) > 1:
                    base.base.object_type = ObjectType.rectangle
                    base.base.contains = list(base.variations.values())

        for reject in rejects:
            del self.bases[reject]

    def _clean_up(self):
        """Mostly removes Inferred objects that were never used"""

        to_remove = []
        # looks for implicit entities with no relevant parents or children.
        for base_word, base in self.bases.items():
            valid_child = any(c.object_type in [ObjectType.entity, ObjectType.control, ObjectType.actor] for c in base.base.ordered_children)
            valid_parent = any(p.object_type in [ObjectType.usecase, ObjectType.control, ObjectType.entity] for p in base.base.ordered_parents)
            
            if not base.base.explicit and not (valid_child or valid_parent) and not base.variations:
                to_remove.append(base_word)

            unconnected_variations = []
            for var, do in base.variations.items():
                if not do.ordered_children and not do.ordered_parents and not do.explicit:
                    unconnected_variations.append(var)
            for var in unconnected_variations:
                try:
                    base.base.contains.remove(base.variations[var])
                except ValueError:
                    pass
                del base.variations[var]

        # for the above entities properties become entities and gain the removed entities children
        for tr in to_remove:
            do = self.bases[tr].base
            t = []
            for p in do.ordered_parents:
                t.append(p)
                if p.composite_of(do):
                    p.object_type = ObjectType.entity
                    for c in do.ordered_children:
                        p.add_child(c)
            do.disconnect()
            del self.bases[tr]
   

    def _create_diagram_objects(self):
        """
        Add au dico self.entity_to_object l'ensemble des diagram_objets basés sur les entitées qui ne sont pas de type proxys ou extensions
        Add au dico self.base un new BaseObject pour chaque spécialisation d'objet qu'il n'a pas encore.
            Si l'entity n'a pas de properties ou self._display_all ==False ou que l'entity n'est pas de type object :
                On ajoute base.all_instances les objets concerné par la spécialisation 
                Add à self.objects l'object
        On applique ca pour toutes les US
        """
        # build initial diagram objects
        story_number = 0
        for ents, pa in self.ents_by_story:
            invalid = EntityType.proxies() + [EntityType.extension]# + ([] if self._display_all else [EntityType.property])
            for source in [e for e in ents if e.entity_type not in invalid]:
                do = DiagramObject(source=source)
                self.entity_to_object[source] = do
                
                if do.specialisation in self.bases:
                    base = self.bases[do.specialisation]
                else:
                    base = BaseObject()
                    self.bases[do.specialisation] = base
                do.add_ref_id(story_number)

                # leaves an empty base up for checking against if necessary.
                if not (source.properties and self._display_all and source.entity_type == EntityType.object):
                    base.all_instances.append(do)
                    self.objects.add(do, story_number, source in pa)
            story_number += 1

    def _connect_implicit_objects(self):
        # connect any extensions
        for extension in [e for e in self.raw if e.entity_type == EntityType.extension]:
            if extension.lemma in self.bases:
                self.entity_to_object[extension] = self.bases[extension.lemma].base
        # connect any implicit objects (The implicit reference may have come before the explicit)
        for implicit in [e for e in self.raw if not e.referenced_explicitly]:
            if implicit.lemma in self.bases:
                self.entity_to_object[implicit] = self.bases[implicit.lemma].base

    def _connect_diagram_objects(self):
        """
        We will connect all the DOs according to the children of the entity that created them.
        For each Control that does not have an interface, we will create one and connect it.
        """
        # create relationships
        for entity in [e for e in self.raw if e.entity_type not in [EntityType.extension] + EntityType.proxies()]:
            self._search_for_children(entity, None)

        # create interfaces
        self._create_boundaries()

        self._connect_usecases()

    def _create_boundaries(self):
        # Sometimes there a references to controls without any actors. These are left alone if they are not tagged
        # as explicit because they are often not a system interaction (i.e. most relations in usecases).
        """
        On infère une interface pour chaque controleur qui n'est encore connecté à aucune.
        Pour la créé, on se base sur les DO de type Entity lié au Control.
        On lie cette nouvelle interface au  Control et aux acteurs lié au Control
        """
        
        # case object control : (explicit or with an actor in children) and is not linked to any boundary 
        for control in [c for c in self.objects[ObjectType.control] if ((c.explicit or any(a for a in c.ordered_children if a.object_type == ObjectType.actor)) and not any(b for b in c.ordered_children if b.object_type == ObjectType.boundary))]:

            # This connects each entity in a relation to its immediate neighbour. E.G. Add a photo to my profile page.
            # connects (photo) --> (profile page). This isn't always correct IMO but the overwhelming majority of the
            # time it's valid.
            e = [c for c in control.ordered_children if c.object_type in [ObjectType.entity, ObjectType.circle]]
            for i in range(0, len(e)-1):
                if e[i] not in e[i+1].children:
                    e[i].add_child(e[i+1])


            # get actors for control, if none exist use the primary actors.
            actors = [a for a in control.ordered_children + control.ordered_parents if a.object_type == ObjectType.actor]
            if not actors:
                # if there is a control as a parent assume that this is a sub-control not directly accessed by actor
                # also means there is no need to create an interface.
                actors = self.objects.primary_actors_for(control)

            candidates = control.interface_candidates()
            
            # if we don't found a candidate and we have "control" in our parents we will their candidate.
            #Exple : "I want to help to create a profile" => help haven't candidate so we use the candidate from create so our candidates is profile
            if (candidates == []):
                children_control = [c for c in control.ordered_parents if c.object_type == ObjectType.control]
                for c in children_control:
                    candidates+= c.interface_candidates()
                
            # checking for interfaces that all ready exist
            interfaces = [i for i in candidates if i.object_type == ObjectType.boundary and i.explicit]

            # if there are no entities or boundaries then there is nothing to infer an interface from.
            ents = [i for i in candidates if i.object_type == ObjectType.entity]
            if not ents and not interfaces:
                # ents = [control]
                continue 
 
            # This checks to see if there is any entity containing the control's name is any of the entities. This is a
            # good indicator it is the boundary. E.g. If you are going to "hammer a nail with a hammer" or
            # "login through a login page" then the nouns (hammer and login page) likely refer to the verbs
            # (hammer & login) and are appropriate interfaces.
            if not interfaces:
                ent = ents[0]
                interfaces = [i for i in candidates if i.object_type == ObjectType.entity and (
                    ent.root in i.descriptors or control.root in i.descriptors or control.root in i.root
                )]
            
            if not interfaces:
                # note that .interface_candidates orders potential candidates from closest connection to furthest so
                # if there is (relation) --> (property) --> (entity) then ents[-1] returns (entity). This prevents
                # there from being too many interfaces created.

                ent = ents[-1]
                ipn = ent.root.title() + "\\nInterface"
                spec = sub(r"[\W+]", "", ent.root.title()).lower() + "interface"
                interface = DiagramObject()
                interface.pretty_name = ipn
                interface._specialisation = spec
                interface.root = self
                interface.explicit = False

                if interface.specialisation not in self.bases:
                    base = BaseObject()
                    self.bases[interface.specialisation] = base
                    
                    self.objects.add(interface, self.objects.do_to_num[control])
                else:
                    base = self.bases[interface.specialisation]

                base.all_instances.append(interface)
                self.objects.add(interface, self.objects.do_to_num[control])

                interfaces.append(interface)

            # This is for nested boundaries. Sometimes a thing is mentioned prior to an relation. "I want the option to"
            # or "I want a button that can" These are put between the inferred interface and the control.
            nb = [b for b in control.ordered_children if b.object_type == ObjectType.boundary and not b.explicit]
            
            for i in interfaces:
                i.object_type = ObjectType.boundary
                for actor in actors:
                    control.remove_child(actor)
                    i.add_child(actor)
                if self._display_all and nb:
                    for b in nb:
                        b.add_child(i)
                else:
                    control.add_child(i)

    def _connect_usecases(self):
        """
        Because usecases in so that sections are disjoint from the rest of the diagram this looks for places
        to connect them to.
        """

        loose_usecases = [u for u in self.objects[ObjectType.usecase] if not (
            any(c for c in u.ordered_children if
                c.explicit and c.object_type in [ObjectType.usecase, ObjectType.control])
        )]

        # This returns any control or usecase with no parents, in other words a terminal node in the graph.
        tns = [tn for tn in self.objects.all if (
                tn.object_type in [ObjectType.control, ObjectType.usecase] and
                not any(p.object_type == ObjectType.usecase for p in tn.ordered_parents) and
                tn not in loose_usecases and
                tn.explicit
        )]

        for usecase in loose_usecases:
            for terminal_node in [tn for tn in tns if self.objects.do_to_num[tn] == self.objects.do_to_num[usecase]]:
                # check the reverse relationship does not already exist
                if usecase.specialisation not in terminal_node.children:
                    usecase.add_child(terminal_node)

    def _collapse_final(self, final: Entity, sup: DiagramObject) -> DiagramObject:
        temp = Property(final.lemma)
        temp.super_property = copy(final.properties[0]) if final.properties else None
        variants = temp.all_variants()
        for variant, remainder in variants:
            do = DiagramObject(source=variant, object_type=ObjectType.circle if sup else ObjectType.entity)
            if do.specialisation in self.bases:
                if remainder:
                    r = DiagramObject(source=remainder, object_type=ObjectType.circle if sup else ObjectType.entity)
                    self.loose_diagram_objects.append(r)
                    if sup:
                        r.add_child(sup)
                    sup = r
                self.bases[do.specialisation].all_instances.append(do)
                if sup:
                    #Exemple : "a user profile : do = profile and sup = user"
                    #Here profile compose user, so profile is an entity property
                    do.is_entity_property = True
                    do.add_child(sup)
                return do

        do = DiagramObject(temp, object_type=ObjectType.circle if sup else ObjectType.entity)
        do.explicit = not sup
        if sup:
            do.add_child(sup)
        if do.specialisation not in self.bases:
            self.bases[do.specialisation] = BaseObject()
        self.bases[do.specialisation].all_instances.append(do)
        # Properties HAVE TO BE REMOVED or else the program will try to parse them again.
        final.remove_property()
        return do

    def _make_properties(self, prop: Property, final: Entity, sup: DiagramObject) -> Tuple[DiagramObject, DiagramObject]:
        """
        When we can find the specialisation of the prop in self.base 
        (It's when the prop is connected to entity who exist in the Diagram)
        (Quand l'entité à laquel réfère la propriété existe déjà dans le diagram, on sépare la prop de l'entité en créant 2 Diagram Objet)
        Return 2 DiagramObject
            first for the prop 
            second for the entity
        In the other case, we return 2 similar DiagramObject. 
    

        Exmple : 
            employee is a DiagramObjet
            final =  [Implicit EntityType.object] {} ()  <EMPLOYEE(None)> access || access 
            first_return = circle "Access" as thingaccess #grey
            second_return = entity "Employee" as thingemployee #grey

            Other case : 
            contract is not a DiagramObjet
            entity  [Explicit EntityType.object] {} ()  <CONTRACT(None)> length || length
            first_return = entity "Contract Length" as thingcontractlength #grey 
            second_return = entity "Contract Length" as thingcontractlength #grey
        """
        variants = prop.all_variants()
        for variant, remainder in variants:
            do = DiagramObject(source=variant, object_type=ObjectType.circle if sup else ObjectType.entity)
            

            if do.specialisation in self.bases:
                if remainder:
                    r = DiagramObject(source=remainder, object_type=ObjectType.circle if sup else ObjectType.entity)
                    self.loose_diagram_objects.append(r)
                    if sup:
                        r.add_child(sup)
                    sup = r

                self.bases[do.specialisation].all_instances.append(do)
                if sup:
                    do.add_child(sup)
                if prop.sub_property:
                    prop.sub_property.super_property = None
                    return self._make_properties(prop.sub_property, final, do)
                else:
                    final.remove_property()
                    return self._collapse_final(final, do), sup if sup else do
        if prop.sub_property:
            return self._make_properties(prop.sub_property, final, sup)
        else:
            result = self._collapse_final(final, sup)
            return result, sup if sup else result

    def _parse_properties(self, entity: Entity, last_object: Optional[DiagramObject]):
        """
        Creation of DO representing the properties of an Entity if necessary
        """
        bottom, top = self._make_properties(entity.base_property(), entity, None)
        
        entity.remove_property()
        # top/bottom is assigned to entity_to_object based on if this node is being visited from below first
        # (i.e. to connect to relation/usecase) use top so it can be connected to any parents. If starting from the
        # the entity then the bottom is the reference so relations can connect to it.
        if last_object:
            self.entity_to_object[entity] = top

            last_object.add_child(bottom)

            if last_object.object_type in [ObjectType.control, ObjectType.usecase]:
                for relation in [r for r in entity.relations if r.entity_type == EntityType.extension]:
                    self._search_for_children(relation, last_object)
        else:
            self.entity_to_object[entity] = bottom

    def _search_for_children(self, entity: Entity, last_object: Optional[DiagramObject]):
        """
        This searches all of the relations for an entity to connect up the relevant DiagramObjects. Not that the order
        in which entities are searched is completely random. This is why there are frequent return statements (so that
        it only ever really goes one layer deep into the enity relations, otherwise starting from different points
        would produce different results. This interaction may not be necessary as it's largely fixed thanks to the new
        property system but it remains this way just in case. Any changes to this method HAS to maintain the order
        independent parsing. If you are seeing loops or duplicated relations in the diagram this is a possible source.

        :param entity: Entity to begin search from.
        :param last_object: The last object looked at.
        """
        
        if self._display_all and entity.properties and entity.entity_type == EntityType.object:
            self._parse_properties(entity, last_object)
            return
        elif entity in self.entity_to_object:
            do = self.entity_to_object[entity]

            if last_object:

                if do.object_type == ObjectType.control:
                    if last_object.object_type in [ObjectType.control, ObjectType.boundary, ObjectType.actor]:
                        do.add_child(last_object)
                        last_object = do
                    elif last_object.object_type in [ObjectType.usecase] and self._display_all:
                        last_object.add_child(do)
                    else:
                        return

                elif do.object_type == ObjectType.entity:
                    if last_object.object_type == ObjectType.entity:

                        if last_object.composite_of(do):
                            if self._display_all:
                                last_object.object_type = ObjectType.circle
                            last_object.add_child(do)
                        else:
                            last_object.add_child(do)
                            return
                    elif last_object.object_type == ObjectType.control:
                        # prevents relation -> ent -> ent from cascading.
                        # if entity.parent.entity_type in [EntityType.relation, EntityType.extension]:
                        last_object.add_child(do)
                        for relation in [r for r in entity.relations if r.entity_type == EntityType.extension]:
                            self._search_for_children(relation, last_object)
                        return

                    elif last_object.object_type == ObjectType.usecase:
                        if self._display_all:
                            last_object.add_child(do)
                        for relation in [r for r in entity.relations if r.entity_type == EntityType.extension]:
                            self._search_for_children(relation, last_object)
                        return

                    elif last_object.object_type == ObjectType.circle:
                        # if last_object.composite_of(do):
                        last_object.add_child(do)
                        return

                    else:
                        return

                elif do.object_type == ObjectType.actor:
                    if last_object.object_type not in [ObjectType.boundary, ObjectType.control]:
                        return
                    else:
                        last_object.add_child(do)
                        last_object = do

                elif do.object_type == ObjectType.circle:
                    if last_object.object_type == ObjectType.entity:

                        if last_object.composite_of(do):
                            if self._display_all:
                                last_object.object_type = ObjectType.circle
                            last_object.add_child(do)
                        else:
                            last_object.add_child(do)
                            return
                    elif last_object.object_type == ObjectType.control:
                        # prevents relation -> ent -> ent from cascading.
                        # if entity.parent.entity_type in [EntityType.relation, EntityType.extension]:
                        last_object.add_child(do)
                        for relation in [r for r in entity.relations if r.entity_type == EntityType.extension]:
                            self._search_for_children(relation, last_object)
                        return
                    return

                elif do.object_type == ObjectType.boundary:
                    if last_object.object_type == ObjectType.control:
                        last_object.add_child(do)

                elif do.object_type == ObjectType.usecase:
                    if last_object.object_type in [ObjectType.usecase, ObjectType.control]:
                        do.add_child(last_object)
                        last_object = do
                    if last_object.object_type in [ObjectType.circle, ObjectType.entity] and self._display_all:
                        last_object.add_child(do)
                    else:
                        return

                else:
                    last_object.add_child(do)
                    last_object = do
            else:
                last_object = do

        for relation in entity.relations:
            self._search_for_children(relation, last_object)

    def _convert_is_entity_property_to_cirle(self):
        """
        All DOs that are entities and are to become properties are converted to Circle
        """
        for do in self._all_list_do:
            if(do.object_type == ObjectType.entity and do.is_entity_property):
                do.object_type = ObjectType.circle
                
    def _create_textual_representation(self,list_do,notes = None):
        """
        Created the planUML code for the list of diagram_objects contained in list_do and add the notes related to the diagram_objects.
        Return code planUML  and US use in the diagram
        """
  

        textual_representation = "\n@startuml\n" \
                                        "\tskinparam defaultTextAlignment center\n" \
                                        "\tskinparam monochrome true\n\n"
        
    
        dic_do={}                                        
        for do in list_do:
            if(do.specialisation in dic_do):
                dic_do[do.specialisation].merge(do)
            else: 
                dic_do[do.specialisation] = do

        list_do = [dic_do[k] for k in dic_do]

        ids_notes = []                                   
        for do in [e for e in list_do if e.object_type != ObjectType.actor] :
            ids_notes += do.ref_ids_notes
        ids_notes = list(set(ids_notes))

        notes_use = []
        if notes:
            ids_notes = [i for i in ids_notes if i <len(notes)]
            notes_use = list(set(np.array(notes)[ids_notes]))
            #textual_representation += self._note_representation(list(set(np.array(notes)[ids_notes])))

        dic_do = {}
        for do in [root for root in list_do]:
            textual_representation += do.string_initializer(display_all=self._display_all)

        textual_representation += "\n"
        default_arrow, directed_arrow, dependency_arrow, composition_arrow = " --- ", " --> ", " <.. ", " *-- "
        directed_types = [ObjectType.control, ObjectType.usecase, ObjectType.boundary]

        for do in list_do:
            for child in do.ordered_children:
                if(child in list_do):
                    show_link = True
                    arrow = default_arrow
                    if all(d.object_type in directed_types for d in [do, child]):
                        arrow = directed_arrow
                    elif child.object_type in [ObjectType.entity, ObjectType.circle] and (do.object_type == ObjectType.circle or do.is_entity_property):
                        arrow = composition_arrow
                    elif all(d.object_type in [ObjectType.entity, ObjectType.boundary] for d in [do, child]) or \
                            (do.object_type == ObjectType.entity and child.object_type == ObjectType.circle):
                        arrow = dependency_arrow

                    #Impossible link
                    if(child.object_type == ObjectType.actor and do.object_type != ObjectType.boundary):
                        show_link = False
                    elif (child.object_type in [ObjectType.entity, ObjectType.circle] and do.object_type in [ObjectType.boundary,ObjectType.actor] ) or \
                        (do.object_type in [ObjectType.entity, ObjectType.circle] and child.object_type in [ObjectType.boundary,ObjectType.actor] ) :
                        show_link = False

                    if (self._display_all or arrow in [default_arrow, directed_arrow]) and show_link:
                        textual_representation += "\t" + child.specialisation  + arrow + do.specialisation + "\n"
                        
        textual_representation += "\n@enduml"
        return textual_representation, notes_use
    
    def _create_subset_diagram_object(self):
        """
        Created the list of diagram_object sets that make up a separate diagram.
        Each set is composed of all the diagram_objects linked together except the actors to avoid having a single diagram.
        The list is in self.subset_diagram_object.
        """
        
        list_set = []
        for do in self._all_list_do:
            list_set.append(set([do]+[child for child in do.ordered_children]))

        merge_list_set = list_set
        new_merge = self._merge_set(list_set)

        #We merge the sets until there are more with diagram_objects in common.
        while(new_merge != merge_list_set):
            merge_list_set = new_merge
            new_merge = self._merge_set(merge_list_set)

        self.subset_diagram_object = new_merge
       

    def _merge_set(self,list_set):
        """
        Checks for each set if there is another set with a common diagram_objects in the following sets (except actor). 
        When he finds one, he merges them and places them in the first set and replaces the other with an empty set. 
        Returns the new overview list by removing the gaps.
        """
        list_set = [c for c in list_set if len(c)!=0]
        for i in range(0,len(list_set)):
            cluster1 = list_set[i]
            for j in range(i,len(list_set)):
                cluster2 = list_set[j]
                if([e for e in cluster1.intersection(cluster2) if e.object_type != ObjectType.actor]):
                    cluster1 = set(list(cluster1)+list(cluster2))
                    cluster2 = set([])
                list_set[j]=cluster2
            
            list_set[i]=cluster1
        
        return [c for c in list_set if len(c)!=0]

    def _diagram_for_actor(self,view_do):
        """
        viewdo is an actor.
        Returns the list of diagram_object that composes the view_do diagram 
        """
        result = set([view_do])
        add_set_boundary = [d for d in view_do.ordered_parents  if d.object_type not in [ObjectType.actor,ObjectType.entity,ObjectType.circle,ObjectType.control]]
        result = result.union(add_set_boundary)

        add_set_control = set([])
        for boundary in add_set_boundary :
            new_control = [d for d in boundary.ordered_parents  if d.object_type not in [ObjectType.actor,ObjectType.entity,ObjectType.circle,ObjectType.boundary]]
            add_set_control = add_set_control.union(new_control)
        result = result.union(add_set_control)

        end = False
        add_set_entity_control_circle = set([])
        do_boucle = add_set_control
        while(not end):
            for control in do_boucle :
                new_entity_control_circle = [d for d in control.ordered_children if d.object_type not in [ObjectType.actor,ObjectType.boundary]]
                add_set_entity_control_circle = add_set_entity_control_circle.union(new_entity_control_circle)
            do_boucle = add_set_entity_control_circle
            if(result.union(add_set_entity_control_circle) == result):
                end = True
            result = result.union(add_set_entity_control_circle)
            
        return result

    def _diagram_for_boundary(self,view_do):
        """
        view_do is a boundary.
        Returns the list of diagram_object that composes the view_do diagram 
        """
        result = set([view_do])

        add_set_actor = [d for d in view_do.ordered_children  if d.object_type not in [ObjectType.boundary,ObjectType.entity,ObjectType.circle,ObjectType.control]]
        result = result.union(add_set_actor)

        add_set_control = [d for d in view_do.ordered_parents  if d.object_type not in [ObjectType.actor,ObjectType.entity,ObjectType.circle,ObjectType.boundary]]
        result = result.union(add_set_control)

        end = False
        add_set_entity_control_circle = set([])
        do_boucle = add_set_control
        while(not end):
            for control in do_boucle :
                new_entity_control_circle = [d for d in control.ordered_children if d.object_type not in [ObjectType.actor,ObjectType.boundary]]
                add_set_entity_control_circle = add_set_entity_control_circle.union(new_entity_control_circle)
            do_boucle = add_set_entity_control_circle
            if(result.union(add_set_entity_control_circle) == result):
                end = True
            result = result.union(add_set_entity_control_circle)
        return result

    def _diagram_for_entity(self,view_do):
        """
        view_do is an entity.
        Returns the list of diagram_object that composes the view_do diagram 
        """
        result = set([view_do])
        do_boucle = set([view_do])
        end = False
        add_set_entity_control_circle = set([])

        while(not end):
            for control in do_boucle :
                new_entity_control_circle = [d for d in control.ordered_parents if d.object_type not in [ObjectType.actor,ObjectType.boundary]]
                add_set_entity_control_circle = add_set_entity_control_circle.union(new_entity_control_circle)
            do_boucle = add_set_entity_control_circle
            if(result.union(add_set_entity_control_circle) == result):
                end = True
            result = result.union(add_set_entity_control_circle)
            
        add_set_boundary = set([])
        controls = [do for do in result if do.object_type == ObjectType.control]
        for control in controls :
            new_boundary = [d for d in control.ordered_children  if d.object_type not in [ObjectType.actor,ObjectType.entity,ObjectType.circle,ObjectType.control]]
            add_set_boundary = add_set_boundary.union(new_boundary)
        result = result.union(add_set_boundary)

        add_set_actor = set([])
        for boundary in add_set_boundary:
            new_actor = [d for d in boundary.ordered_children  if d.object_type not in [ObjectType.boundary,ObjectType.entity,ObjectType.circle,ObjectType.control]]
            add_set_actor = add_set_actor.union(new_actor)
        result = result.union(add_set_actor)

        return result
        


    def textual_representation_by_view(self,view_do):
        """
        if view_do must is an actor or entity or boundary 
            Returns the plan_uml code to generate the view_do diagram
        else None
        """
        result = set()
        if(view_do.object_type == ObjectType.actor):
            result = self._diagram_for_actor(view_do)
            return self._create_textual_representation(result,notes=self.notes)
        elif(view_do.object_type == ObjectType.boundary):
            result = self._diagram_for_boundary(view_do)
            return self._create_textual_representation(result,notes=self.notes)
        elif(view_do.object_type == ObjectType.entity):
            result = self._diagram_for_entity(view_do)
            return self._create_textual_representation(result,notes=self.notes)
        return None
    

    def _is_valid_set(self,set_):
        """
        To be a valid set, it must contain at least one interface and one control.
        """
        boundary_in = False
        control_in = False
        for e in set_:
            if(e.object_type == ObjectType.boundary):
                boundary_in=True
            elif(e.object_type == ObjectType.control):
                control_in = True
        return boundary_in and control_in
    
    def _merge_similarity_diagram_object(self):
        """
        Merges all object diagrams with the same specialization.
        Merge all diagram_object (Actor, Entity and Circle) by type that are considered similar. 
        Merge all diagram_object (Boundary and Control) by type with the same pretty print.  
        We do not margin by similarity because too many of them are merged and this causes problems. 
        So we just use their pretty print to avoid having several Controllers Or Boundary with similar descriptions in the final m.  
        
        The list of processed object diagrams is in self._all_list_do at the end.  
        """
        wordnet.ensure_loaded()
        #Merges all object diagrams with the same specialization.
        dic_do = {}
        for do in self._all_list_do:
            if(do.specialisation in dic_do):
                dic_do[do.specialisation].merge(do)
            else: 
                dic_do[do.specialisation] = do
        
        list_do_clean = [dic_do[key] for key in dic_do]

        
        list_actor = [do for do in list_do_clean if do.object_type == ObjectType.actor]
        list_entity = [do for do in list_do_clean if do.object_type in  [ObjectType.entity]]
        list_circle = [do for do in list_do_clean if do.object_type in  [ObjectType.circle]]
        list_other = [do for do in list_do_clean if do.object_type not in  [ObjectType.actor,ObjectType.entity,ObjectType.circle, ObjectType.boundary, ObjectType.control]]
        
        list_boundary = [do for do in list_do_clean if do.object_type in  [ObjectType.boundary]]
        list_control = [do for do in list_do_clean if do.object_type in  [ObjectType.control]]
        
        #Merge all diagram_object (Actor, Entity and Circle) by type that are considered similar. 

        list_entity = Diagram._merge_similarity(list_entity)
        list_circle = Diagram._merge_similarity(list_circle)
        list_actor = Diagram._merge_similarity(list_actor)

        #Merge all diagram_object (Boundary and Control) by type with the same pretty print.  
        list_boundary = Diagram._merge_by_pretty_name(list_boundary)
        list_control = Diagram._merge_by_pretty_name(list_control)

        self._all_list_do = list_entity + list_actor + list_other + list_circle + list_boundary + list_control

        
    def _merge_similarity(list_do):
        """
        We merge the DOs that for each of the words that make up their pretty_name, 
        there is a similar word in the pretty_name of the other DO.
        Return the list_do with the merged objects

        """

        for i in range(0,len(list_do)):
            if(list_do[i]):
                d1 = list_do[i].pretty_name.replace("\\n"," ").lower()
                for j in range(i+1,len(list_do)):
                    if(list_do[j]):
                        d2 = list_do[j].pretty_name.replace("\\n"," ").lower()

                        if(Diagram._is_similar_description(d1,d2)):
                            print("ON MERGE : ",d1," - ",d2)
                            list_do[i].merge(list_do[j])
                            list_do[j] = None
        return [do for do in list_do if do != None]

    def _merge_by_pretty_name(list_do):
        """
        We merge the DOs from list_do that have the same pretty_name
        Return the list_do with the merged objects
        """
        for i in range(0,len(list_do)):
            if(list_do[i]):
                d1 = list_do[i].pretty_name.replace("\\n"," ").lower()
                for j in range(i+1,len(list_do)):
                    if(list_do[j]):
                        d2 = list_do[j].pretty_name.replace("\\n"," ").lower()
                        if(d1 == d2):
                            list_do[i].merge(list_do[j])
                            list_do[j] = None
        return [do for do in list_do if do != None]  


    def _is_similar_description(s1,s2,sim_min = 0.7):
        """
        Returns True if each of the words in s1 finds at least one similar word in s2 and the same condition for s2 to s1.
        Otherwise False
        """
        doc1 = nlp(s1)
        doc2 = nlp(s2)
        if(s1 == "" or s2 == ""):
            return False
        try:
            
            for tok1 in doc1:
                sim = False
                if (tok1 and tok1.vector_norm):
                    for tok2 in doc2:
                        if (tok2 and tok2.vector_norm):
                            if tok1.similarity(tok2)>=sim_min and Diagram._is_synonyms(tok1.text,tok2.text):
                                print("sim_detect ",tok1, " - ",tok2," : ",tok1.similarity(tok2))
                                sim = True
                    if(not sim):
                        return False
                
            for tok2 in doc2:
                sim = False
                if (tok2 and tok2.vector_norm):
                    for tok1 in doc1:
                        if (tok1 and tok1.vector_norm):
                            if tok1.similarity(tok2)>=sim_min and Diagram._is_synonyms(tok1.text,tok2.text):
                                sim = True
                    if(not sim):
                        return False
            return True
        except TypeError:
            return False

    def _is_synonyms(w1,w2):
        """
        Returns True if w1 is part of the synonyms of w2
        Otherwise False
        """
        synonyms = [] 
        for syn in wordnet.synsets(w1): 
            for l in syn.lemmas(): 
                synonyms.append(l.name())
        return w2.lower() in synonyms

    def count_do(self):
        """
        Returns the number of DO, number of connected DO and number of unconnected DO
        """

        nb_do_tot = len(self._all_list_do)

        nb_do_connect = 0
        nb_do_unconnect = 0

        for do in self._all_list_do:
            in_set = False
            #Verifies that the object is part of a valid diagram.
            for set_do in [set_do for set_do in self.subset_diagram_object if self._is_valid_set(set_do)]:
                if (do.specialisation in [d.specialisation for d in set_do]):
                    in_set = True
            if in_set:
                nb_do_connect+=1
            else : 
                nb_do_unconnect+=1

        return nb_do_tot,nb_do_connect,nb_do_unconnect


   
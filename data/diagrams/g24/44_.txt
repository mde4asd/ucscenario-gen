
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Project" as thingproject
	entity "Metadata" as thingmetadata
	entity "Dataset" as thingdataset
	actor "Research ,\nInformation Manager" as actorresearchinformationmanager
	boundary "Project\nInterface" as thingprojectinterface #grey
	control "Have Datasets\nTo Metadata\nAbout Projects" as controlhavedataset

	thingproject <.. thingmetadata
	thingmetadata <.. thingdataset
	actorresearchinformationmanager --- thingprojectinterface
	thingdataset --- controlhavedataset
	thingmetadata --- controlhavedataset
	thingproject --- controlhavedataset
	thingprojectinterface --> controlhavedataset

@enduml
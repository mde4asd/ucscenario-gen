
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Collection" as thingcollection
	entity "Creator" as thingcreator
	entity "Person" as thingperson
	entity "Subject" as thingsubject
	entity "Corporate Body" as thingcorporatebody
	entity "Family" as thingfamily
	entity "Source" as thingsource
	actor "Repository Manager" as actorrepositorymanager
	boundary "Creator\nInterface" as thingcreatorinterface #grey
	boundary "Subject\nInterface" as thingsubjectinterface #grey
	boundary "Source\nInterface" as thingsourceinterface #grey
	boundary "Person\nInterface" as thingpersoninterface #grey
	boundary "Family\nInterface" as thingfamilyinterface #grey
	boundary "Body\nInterface" as thingbodyinterface #grey
	control "Indicate Names\nTo The\nCollection As\nCreator" as controlindicatename
	control "Indicate Names\nTo The\nCollection As\nSubject" as controlindicatenamesubject
	control "Indicate Names\nTo The\nCollection As\nSource" as controlindicatenamesource
	control "Indicate Person" as controlindicateperson
	control "Indicate Family" as controlindicatefamily
	control "Indicate Corporate\nBody" as controlindicatecorporatebody

	thingcreator <.. thingcollection
	thingsource <.. thingcollection
	thingsubject <.. thingcollection
	actorrepositorymanager --- thingcreatorinterface
	actorrepositorymanager --- thingsubjectinterface
	actorrepositorymanager --- thingsourceinterface
	actorrepositorymanager --- thingpersoninterface
	actorrepositorymanager --- thingfamilyinterface
	actorrepositorymanager --- thingbodyinterface
	thingcollection --- controlindicatename
	thingcreator --- controlindicatename
	thingcreatorinterface --> controlindicatename
	thingsubject --- controlindicatenamesubject
	thingsubjectinterface --> controlindicatenamesubject
	thingcollection --- controlindicatenamesubject
	thingsource --- controlindicatenamesource
	thingsourceinterface --> controlindicatenamesource
	thingcollection --- controlindicatenamesource
	thingperson --- controlindicateperson
	thingpersoninterface --> controlindicateperson
	thingfamily --- controlindicatefamily
	thingfamilyinterface --> controlindicatefamily
	thingcorporatebody --- controlindicatecorporatebody
	thingbodyinterface --> controlindicatecorporatebody

@enduml
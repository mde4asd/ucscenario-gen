
@startuml
	skinparam defaultTextAlignment center
	skinparam monochrome true

	entity "Dataset" as thingdataset
	entity "Source File" as thingsourcefile #grey
	entity "Files Change" as thingfileschange #grey
	actor "Data ,\nPublishing User" as actordatapublishinguser
	boundary "Change\nInterface" as thingchangeinterface #grey
	boundary "File\nInterface" as thingfileinterface #grey
	control "Have Dataset\nAs Files\nChange" as controlhavedatasetfileschange
	control "Have Dataset\nAs The\nSource File" as controlhavedatasetsourcefile

	thingsourcefile <.. thingdataset
	thingfileschange <.. thingdataset
	actordatapublishinguser --- thingchangeinterface
	actordatapublishinguser --- thingfileinterface
	thingfileschange --- controlhavedatasetfileschange
	thingchangeinterface --> controlhavedatasetfileschange
	thingdataset --- controlhavedatasetfileschange
	thingdataset --- controlhavedatasetsourcefile
	thingsourcefile --- controlhavedatasetsourcefile
	thingfileinterface --> controlhavedatasetsourcefile

@enduml